
// /etc/lxpresenter  ==>  ncrun will run evince !!

/* gameboy
<CMD:     /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-gambatte/gambatte_libretro.so --config /opt/retropie/configs/gb/retroarch.cfg      "megaman1.gb"     --appendconfig /dev/shm/retroarch.cfg  >
sh: 1: /opt/retropie/emulators/retroarch/bin/retroarch: not found
need mednafen 
*/


///////////////////////
// NCRUN 
// Libre Software, MIT 
///////////////////////

#include <stdio.h> 
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include <dirent.h> 
#include <ctype.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>  
#include <time.h>
#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif
#define KRED  "\x1B[31m"
#define KGRE  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KNRM  "\x1B[0m"
#define KBGRE  "\x1B[92m"
#define KBYEL  "\x1B[93m"


///#define PATH_MAX 4096
int var_current_path_local = 1;
int foundcmd = 0; 


int mediacenter_fullscreen = 0; 




void nsystem( char *mycmd )
{
	printf( "<CMD: %s>\n", mycmd );
	system( mycmd );
	printf( " Defunc CMD: %s>\n", mycmd );
}




///////////////////////////////////////
///////////////////////////////////////
int fexist(const char *a_option)
{
  char dir1[PATH_MAX]; 
  char *dir2;
  DIR *dip;
  strncpy( dir1 , "",  PATH_MAX  );
  strncpy( dir1 , a_option,  PATH_MAX  );

  struct stat st_buf; 
  int status; 
  int fileordir = 0 ; 

  status = stat ( dir1 , &st_buf);
  if (status != 0) {
    fileordir = 0;
  }
  FILE *fp2check = fopen( dir1  ,"r");
  if( fp2check ) {
  fileordir = 1; 
  fclose(fp2check);
  } 

  if (S_ISDIR (st_buf.st_mode)) {
    fileordir = 2; 
  }
return fileordir;
/////////////////////////////
}









void app_mupdf(  char *filesource )
{
           char cmdi[PATH_MAX];
           strncpy( cmdi , "  " , PATH_MAX );

	   if ( fexist( "/usr/pkg/bin/mupdf" ) == 1 ) 
	   {
	       printf( "netbsd...\n" );
               strncat( cmdi , " /usr/pkg/bin/mupdf " , PATH_MAX - strlen( cmdi ) -1 );
	   }


	   else if ( fexist( "/usr/bin/mupdf-x11" ) == 1 ) 
	   {
	       printf( "Slackware...\n" );
               strncat( cmdi , " /usr/bin/mupdf-x11 " , PATH_MAX - strlen( cmdi ) -1 );
	   }

	   else if ( fexist( "/usr/bin/mupdf" ) == 1 ) 
	   {
	       printf( "linux...\n" );
               strncat( cmdi , " /usr/bin/mupdf " , PATH_MAX - strlen( cmdi ) -1 );
	   }

	   else if ( fexist( "/usr/local/bin/cmmupdf" ) == 1 ) 
	   {
	       printf( " netbsd local with cmmupdf ...\n" );
               strncat( cmdi , " /usr/local/bin/cmmupdf " , PATH_MAX - strlen( cmdi ) -1 );
	   }

	   else if ( fexist( "/usr/local/bin/mupdf" ) == 1 ) 
	   {
	       printf( "FreeBSD...\n" );
               strncat( cmdi , " /usr/local/bin/mupdf " , PATH_MAX - strlen( cmdi ) -1 );
	   }

	   else 
	   {
               strncat( cmdi , " mupdf " , PATH_MAX - strlen( cmdi ) -1 );
	   }

           strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi ,  filesource , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
	   printf( "CMD %s\n" , cmdi );
           nsystem( cmdi ); 
}













void nrunwith( char *mycmd, char *myfile )
{
	char cmdi[PATH_MAX];
        char foocharo[PATH_MAX];
	snprintf( foocharo , sizeof( foocharo ), " %s \"%s\" " , mycmd , myfile ); 
	nsystem(   foocharo ); 
}





/*
void nrunwith( char *cmdapp, char *filesource )
{
           char cmdi[PATH_MAX];
           strncpy( cmdi , "  " , PATH_MAX );
           strncat( cmdi , cmdapp , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi ,  filesource , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
	   printf( "CMD %s\n" , cmdi );
           nsystem( cmdi ); 
}
*/




















char *fbasename(char *name)
{
	char *base = name;
	while (*name)
	{
		if (*name++ == '/')
		{
			base = name;
		}
	}
	return (base);
}






char *fawkdottail(char *name)
{
	char *base = name;
	while (*name)
	{
		if (*name++ == '.' )
		{
			base = name;
		}
	}
	return (base);
}






// nrunwith( "  /usr/games/pcsxr  -nogui -cdfile   ", argv[ i ] );
void movie_player( char *musicurl )
{
		if ( ( fexist( "/usr/bin/mpv" ) == 1 ) && ( mediacenter_fullscreen == 1 ) ) 
		{
	            nrunwith( " /usr/bin/mpv --fs " , musicurl ); 
		}

		else if ( fexist( "/usr/bin/mpv" ) == 1 ) 
	            nrunwith( " /usr/bin/mpv " , musicurl ); 

		else if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
	            nrunwith( " /usr/bin/cvlc  " , musicurl ); 

		else if ( fexist( "/usr/bin/vlc" ) == 1 ) 
	            nrunwith( " /usr/bin/vlc " , musicurl ); 

		else if ( fexist( "/usr/bin/mpv" ) == 1 ) 
	            nrunwith( " /usr/bin/mpv " , musicurl ); 

		else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
	            nrunwith( " /usr/bin/mplayer " , musicurl ); 


		else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
	            nrunwith( " /usr/bin/mplayer -fs -zoom " , musicurl ); 

		else if ( fexist( "/usr/pkg/bin/mplayer" ) == 1 )  // netbsd 
	            nrunwith( " /usr/pkg/bin/mplayer " , musicurl ); 

		else if ( fexist( "/usr/local/bin/mplayer" ) == 1 )  // freebsd 
	            nrunwith( " /usr/local/bin/mplayer " , musicurl ); 

		else if ( fexist( "/usr/bin/totem" ) == 1 ) 
	            nrunwith( " /usr/bin/totem " , musicurl ); 

		else if ( fexist( "/usr/bin/ffplay" ) == 1 ) 
	            nrunwith( " /usr/bin/ffplay " , musicurl ); 

		else if ( fexist( "/usr/bin/totem" ) == 1 ) 
	            nrunwith( " /usr/bin/totem " , musicurl ); 

		else
		   nrunwith( " mplayer  " ,  musicurl ); 
}









void music_player( char *musicurl )
{
		if ( fexist( "/usr/bin/mpv" ) == 1 ) 
	            nrunwith( " /usr/bin/mpv " , musicurl ); 

		else if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
	            nrunwith( " /usr/bin/cvlc  " , musicurl ); 

		else if ( fexist( "/usr/bin/vlc" ) == 1 ) 
	            nrunwith( " /usr/bin/vlc " , musicurl ); 

		else if ( fexist( "/usr/bin/mpv" ) == 1 ) 
	            nrunwith( " /usr/bin/mpv " , musicurl ); 

		else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
	            nrunwith( " /usr/bin/mplayer " , musicurl ); 

		else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
	            nrunwith( " /usr/bin/mplayer -fs -zoom " , musicurl ); 

		else if ( fexist( "/usr/pkg/bin/mplayer" ) == 1 )  // netbsd 
	            nrunwith( " /usr/pkg/bin/mplayer " , musicurl ); 

		else if ( fexist( "/usr/local/bin/mplayer" ) == 1 )  // freebsd 
	            nrunwith( " /usr/local/bin/mplayer " , musicurl ); 

		else if ( fexist( "/usr/bin/ffplay" ) == 1 ) 
	            nrunwith( " /usr/bin/ffplay " , musicurl ); 

		else if ( fexist( "/usr/bin/pragha" ) == 1 ) 
	            nrunwith( " /usr/bin/pragha " , musicurl ); 

		else
		   nrunwith( " mplayer  " ,  musicurl ); 
}





void procedure_pictureviewer(  char * myfoourl )
{
	printf( " =========================\n"  ); 
	printf( " PROCEDURE Picture Viewer \n" ); 
	printf( " =========================\n"  ); 

	char foocharo[PATH_MAX];
	printf( "URL: %s\n" , myfoourl ); 

	if ( fexist( "/usr/bin/gwenview" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/gwenview  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/mupdf" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/mupdf  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/firefox" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/firefox  \"%s\"  " , myfoourl ); 

	printf( " System Command: (%s)\n", foocharo );
	nsystem(  foocharo ); 
}









void procedure_webbrowser(  char * myfoourl )
{
	printf( " =====================\n"  ); 
	printf( " PROCEDURE WEB BROWSER \n" ); 
	printf( " =====================\n"  ); 

	char foocharo[PATH_MAX];
	printf( "URL: %s\n" , myfoourl ); 

	if ( fexist( "/usr/local/bin/firefox" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/firefox  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/chromium-browser" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/chromium-browser --new-window  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/chromium" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/chromium --new-window  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/google-chrome-stable" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/google-chrome-stable \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/pkg/bin/firefox" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/firefox  \"%s\"  " , myfoourl ); 

        /// kde opensuse
	else if ( fexist( "/usr/bin/firefox" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/firefox  \"%s\"  " , myfoourl ); 

        /// kde devuan
	else if ( fexist( "/usr/bin/iceweasel" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/iceweasel  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/galeon" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/galeon  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/bin/midori" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/midori  \"%s\"  " , myfoourl ); 

        // linux netsurf  
	else if ( fexist( "/usr/bin/netsurf" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/netsurf  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/bin/dillo" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/dillo  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/pkg/bin/dillo" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/dillo  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/bin/links2" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/links2 -g \"%s\"  " , myfoourl ); 

        /// lib x11 based
	else if ( fexist( "/usr/local/bin/linksgfx" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/linksgfx  -g  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/xweb" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/xweb  -g  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/cmlinksg" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/cmlinksg  -g  \"%s\"  " , myfoourl ); 

        /// textbased
	else if ( fexist( "/usr/bin/links" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " links  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/links" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/links  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/cmlinks" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/cmlinks  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/pkg/bin/links" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/links  \"%s\"  " , myfoourl ); 

	else 
	   snprintf( foocharo , sizeof( foocharo ), " links  \"%s\"  " , myfoourl ); 
	printf( " System Command: (%s)\n", foocharo );
	nsystem(  foocharo ); 
}










////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
int main( int argc, char *argv[])
{ 

	char cmdi[PATH_MAX];
	char libreoffice_parameter_start[PATH_MAX];
        char foostr[PATH_MAX];
	int i; 

	strncpy( foostr, "", PATH_MAX ); 
	strncpy( libreoffice_parameter_start, "  --nodefault  --nolockcheck --nologo --norestore --nosplash  ", PATH_MAX ); 

	printf( " == NCRUN ==\n" );  

	mediacenter_fullscreen = 0; 
	if ( fexist( "/etc/flde.mediacenter" ) == 1 ) 
	{
	   mediacenter_fullscreen = 1; 
	}







	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "--help" ) ==  0 ) 
	{
		printf(  " == HELP == \n" ); 
		printf(  "  ->  /etc/fltk.mediacenter (%d)\n" ,  mediacenter_fullscreen ); 
		return 0; 
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "--execute" ) ==  0 ) 
	{
		///printf( "%s\n", fbasename( getcwd( cmdi , PATH_MAX ) ) );
                snprintf( foostr, sizeof( foostr ) , "  ./\"%s\" ",  argv[ 2 ] ); 
		nsystem( foostr ); 
		return 0;
	}




	if ( argc >= 3 )
	{
			if ( argc >= 2 )
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else
					{
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
						strncpy( cmdi,  " ", PATH_MAX );
						strncat( cmdi , " xterm -e  ncrun  " , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "  \"" , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "\"  " , PATH_MAX - strlen( cmdi ) -1 );
						printf( "- COMMAND: %s \n", cmdi );
						system( cmdi );
						printf( "- File with process (status: completed).\n" );
					}
				}
			}
			return 0;
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "--pwd-basename" ) ==  0 ) 
	{
		printf( "%s\n", fbasename( getcwd( cmdi , PATH_MAX ) ) );
		return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "--pathmax" ) ==  0 ) 
	{
			printf( "PATH MAX: %d\n",  PATH_MAX );
			return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--extension" ) ==  0 ) 
	{
			printf(  "%s\n" , fawkdottail( argv[ 2 ] ) ); 
			return 0; 
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--console-editor" ) ==  0 ) 
	{
		if ( fexist( "/usr/bin/vim" ) == 1 ) 
			nrunwith( "  /usr/bin/vim  ", argv[ 2 ] );
		else if ( fexist( "/usr/bin/vim-nox11" ) == 1 ) 
			nrunwith( "  vim-nox11   ", argv[ 2 ] );
		else
			nrunwith( "  vim   ", argv[ 2 ] );
		return 0; 
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "vlc" ) ==  0 ) 
	{
             printf( "  cvlc   \n" ); 
             printf( "  cat guitar4.wav | cvlc -q --play-and-exit -  \n" ); 
	     return 0; 
	}




	printf( " == NCRUN == \n" );
	///int i; 



	char romfile[PATH_MAX];




      char cwd[PATH_MAX];
      char fileselection[PATH_MAX];
      strncpy( fileselection, "", PATH_MAX ); 

      char currentpath[PATH_MAX];


      int  driver_mednafen = 0; 
      char filestr[PATH_MAX];

      printf( "Path:%s\n", getcwd( cwd, PATH_MAX) ); 
      strncpy( currentpath, getcwd( cwd, PATH_MAX ), PATH_MAX ); 




	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "-path" ) ==  0 ) 
	|| ( strcmp( argv[1] , "PWD" ) ==  0 ) 
	|| ( strcmp( argv[1] , "pwd" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--path" ) ==  0 ) )
	{
		printf( "Current path: %s\n", getcwd( cwd , PATH_MAX ) );
		return 0;
	}



      if ( argc >= 2)
      if ( strcmp( argv[1] , "" ) !=  0 ) 
      {
	      strncpy( cwd            , argv[1] ,  PATH_MAX );
	      strncpy( fileselection  , argv[1] ,  PATH_MAX );
      }

      if ( argc == 2 )
      if ( strcmp( argv[1] , "" ) !=  0 ) 
      {
		      strncpy( filestr , argv[1] ,  PATH_MAX );
		      printf( "FILE:    %s\n", filestr ); 

		      strncpy( romfile , argv[1] ,  PATH_MAX );
		      printf( "ROMFILE: %s\n", romfile ); 
      }






      /// newer :  ednafen -sound.driver alsa -sound.device plughw:0,0.
      // newer :   /usr/games/mednafen  -video.driver sdl    "dir/motorbike.sms"
      // ii  mednafen                          0.8.D.3-1                    multi-platform emulator, including NES, GB/A, Lynx, PC Engine
      /*
	 mednafen  -vdriver sdl -connect -netplay.host 10.0.0.4 
	 mednafen  -vdriver sdl -loadcd psx 
	 mednafen  -vdriver sdl  -sound.driver alsa -sound.device hw:\"0,0\" -connect -netplay.host 10.0.0.4 
	 mednafen  -vdriver sdl  -sound.driver alsa -sound.device hw:\"1,0\" -connect -netplay.host 10.0.0.4 
	 mednafen  -vdriver sdl  -sound.driver alsa    -sound.device hw:\"1,0\"  
       */
      /*
	 -sounddriver 0 
	 Starting Mednafen 0.8.D.3
	 Compiled against SDL 1.2.14, running with SDL 1.2.14
	 Initializing sound...
	 Unknown sound driver "0".  Supported sound drivers:
	 alsa
	 oss
	 sdl
	 jack
	 dummy
       */




      if ( argc == 3 )
      if ( ( strcmp( argv[1] , "--hatari-ao" ) ==  0 ) || ( strcmp( argv[1] , "--hatari-nosound" ) ==  0 ) )
      {
		      printf( "File: %s\n", cwd );
		      printf( "Ext: st atari... \n");
		      strncpy( cwd , " /usr/local/bin/hatari -t ~/tos102uk.img --sound off  --disk-a " , PATH_MAX );
		      strncat( cwd , " " , PATH_MAX - strlen( cwd ) -1 );
		      strncat( cwd , " \"" , PATH_MAX - strlen( cwd ) -1 );
		      strncat( cwd ,  argv[ 2 ] , PATH_MAX - strlen( cwd ) -1 );
		      strncat( cwd , "\"   " , PATH_MAX - strlen( cwd ) -1 );
		      printf( "CMD %s\n" , cwd );
		      nsystem( cwd ); 
		      return 0;
      }




	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "-psx" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--psx" ) ==  0 )  )
	{
			strncpy( cmdi , "  " , PATH_MAX );
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			        if ( fexist( "/usr/games/pcsxr" ) == 1 ) 
				   nrunwith( "  /usr/games/pcsxr  -nogui -cdfile   ", argv[ i ] );
			        else if ( fexist( "/usr/bin/pcsxr" ) == 1 ) 
				   nrunwith( "  /usr/bin/pcsxr  -nogui -cdfile   ", argv[ i ] );
			}
			printf( " ============== \n "); 
			return 0; 
	}






	if ( argc >= 3)
	if ( ( strcmp( argv[1] , "--wii" ) ==  0 ) || ( strcmp( argv[1] , "--gamecube" ) ==  0 ) )
	{
		strncpy( cmdi , "  " , PATH_MAX );
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			nrunwith( "  /usr/games/dolphin-emu -e   " ,  argv[ i ]  );
		}
		printf( " ============== \n "); 
		return 0; 
	}





        //strncpy( cwd , "   setxkbmap us ; mednafen  -vdriver sdl     " , PATH_MAX );
	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--mednafen" ) ==  0 ) || ( strcmp( argv[1] , "-mednafen" ) ==  0 ) )
	{
		if ( fexist( "/usr/games/mednafen" ) == 1 ) 
		    nrunwith( "   /usr/games/mednafen -video.driver sdl " ,  argv[ 2 ] );
		else if ( fexist( "/usr/bin/mednafen" ) == 1 ) 
		    nrunwith( "   /usr/bin/mednafen -video.driver sdl " ,  argv[ 2 ] );
		else 
		    nrunwith( " mednafen -video.driver sdl " ,  argv[ 2 ] );
		return 0;
	}











	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'x' )
          if ( cwd[strlen(cwd)-2] == 'e' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
		  printf( "   atari800pp -OsXlPath ~/atarixl.rom -Image.1  file.xex \n" ); 
		  printf( " atari800pp -OsXlPath ~/atarixl.rom -Image.1  file.xex \n" ); 
		  printf( " (sound fails): atari800 -xlxe_rom ~/BIOS/atarixl.rom -nosound -cart  file... \n" ); 
		  //  atari800pp -OsXlPath ~/atarixl.rom -Image.1  file.xex

		  printf( " Example on retropie:   atari800pp -OsXlPath /home/pi/atarixl.rom -IgnoreErrors 1 -IgnoreWarnings 1 -AcceptLicence 1 -MonitorOnCrash 0 -Sound SDL -Image.1   PM-09.xex  \n" ); 
		  printf( "< File: %s >\n", fileselection );
		  // on retropie

                 if ( fexist( "/usr/local/bin/atari800pp" ) == 1 )
		 {
	            printf( "Tested on RetroPie on rpi3b \n" ); 
		    printf( "File: %s\n", fileselection );
                    //nrunwith(  "   /usr/local/bin/atari800pp -OsXlPath ~/atarixl.rom -IgnoreErrors 1 -IgnoreWarnings 1 -AcceptLicence 1 -MonitorOnCrash 0 -Sound SDL -Image.1   " , fileselection ); 
		    // on amd64 no sound 
                    nrunwith(  "   /usr/local/bin/atari800pp -OsXlPath ~/atarixl.rom -IgnoreErrors 1 -IgnoreWarnings 1 -AcceptLicence 1 -MonitorOnCrash 0  -Image.1   " , fileselection ); 
		 }

                 else if ( fexist( "/usr/pkg/bin/atari800" ) == 1 )
		 {
	            printf( "Tested on NetBSD on PI \n" ); 
                    // nsystem( " cd ;   atari800 -xlxe_rom  ~/atarixl.rom -run freeroms-main/atari800/boink-8bits.xex   "  ); 
                    nrunwith(  "     /usr/pkg/bin/atari800 -xlxe_rom  ~/atarixl.rom -run  " , fileselection ); 
		 }
		 else
                    //nrunwith(  "   atari800pp -OsXlPath  ~/atarixl.rom -Image.1    " , fileselection ); 
                    nrunwith(  "     atari800 -xlxe_rom  ~/atarixl.rom   -run  " , fileselection ); 
		 return 0; 
	  }











      if ( strstr( currentpath , "nfs" ) != 0 ) 
      {
	      var_current_path_local = 0;
	      printf( "PATH: NFS directory (distant)\n");
      }
      else
      {
	      var_current_path_local = 1;
	      printf( "PATH: local directory (non distant)\n");
      }





      if ( argc == 1 )
      {
          printf( "No parameter given." );
          printf( "\n" ); 
          return 0;
      }







          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'd' )
          if ( cwd[strlen(cwd)-2] == 'b' )
          if ( cwd[strlen(cwd)-1] == 'n' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: dbn ndata file\n");
	     nrunwith( " ndata  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }

          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'e' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 'l' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: eml\n");
	     nrunwith( " flview  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'i' )
          if ( cwd[strlen(cwd)-2] == 'n' )
          if ( cwd[strlen(cwd)-1] == 'i' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: ini\n");
	     nrunwith( " flnotepad  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }








	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-3] == '.' )
          if ( cwd[strlen(cwd)-2] == 'n' )
          if ( cwd[strlen(cwd)-1] == 'b' )
          {
             //   /opt/Wolfram/WolframEngine/11.0/Executables/Mathematica    ElectrodeShapeCalculation.nb  
             printf( "File: %s\n", cwd );
             printf( "Ext: pdf \n");
             nrunwith( "   /opt/Wolfram/WolframEngine/11.0/Executables/Mathematica   " , argv[ 1 ] );
	     foundcmd = 1;
          }






          // pdf  "PDF "pdf 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 'f' )
          {
             printf( " == PDF Area == \n" ); 
             printf( "File: %s\n", cwd );
             printf( "Ext: pdf \n");  // "PDF" 

	      // bug gnome  so we use mupdf 
	     if ( fexist( "/usr/bin/mupdf" ) == 1 ) 
                 nrunwith( " /usr/bin/mupdf " , argv[ 1 ] );

	     else if ( ( fexist( "/usr/bin/evince" ) == 1 ) && ( fexist( "/etc/lxpresenter" ) == 1 ) )
	     {
	         printf( " LXPRESENTER \n" ); 
                 nrunwith( " /usr/bin/evince " , argv[ 1 ] );
	     }

	     else if ( fexist( "/usr/bin/mupdf" ) == 1 ) 
                 nrunwith( " /usr/bin/mupdf " , argv[ 1 ] );

	     else if ( ( MYOS == 1 ) && ( fexist( "/usr/bin/zypper" ) == 1 )  
	     && ( fexist( "/usr/bin/okular" ) == 1 ) )
	     {
                 nrunwith( " /usr/bin/okular " , argv[ 1 ] );
	     }
	     else if ( ( MYOS == 1 ) && ( fexist( "/usr/bin/zypper" ) == 1 )  
	     && ( fexist( "/usr/bin/evince" ) == 1 ) )
	     {
                 nrunwith( " /usr/bin/evince " , argv[ 1 ] );
	     }
	     else if ( ( MYOS == 1 ) && ( fexist( "/usr/bin/zypper" ) == 1 )  
	     && ( fexist( "/usr/bin/qpdfview" ) == 1 ) )
	     {
                 nrunwith( " /usr/bin/qpdfview " , argv[ 1 ] );
	     }

	     else if ( fexist( "/usr/bin/okular" ) == 1 ) 
	     {
                 nrunwith( " /usr/bin/okular " , argv[ 1 ] );
	     }

	     else if ( fexist( "/usr/bin/mupdf" ) == 1 ) 
	     {
                 nrunwith( " /usr/bin/mupdf " , argv[ 1 ] );
	     }

	     else if ( fexist( "/usr/bin/evince" ) == 1 ) 
                 nrunwith( " /usr/bin/evince " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/xpdf" ) == 1 ) 
	     {
                 nrunwith( " /usr/bin/xpdf " , argv[ 1 ] );
	     }

             else 
	     {
	             printf( " PDF Layer \n" ); 
		     printf( ">Internal Command app_mupdf for pdf file.\n" ); 
		     app_mupdf( argv[ 1 ] );
	     }
	     /// found here??
	     foundcmd = 1;
          }




          // PDF
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'P' )
          if ( cwd[strlen(cwd)-2] == 'D' )
          if ( cwd[strlen(cwd)-1] == 'F' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: pdf \n");
             nrunwith( " mupdf " , argv[ 1 ] );
	     foundcmd = 1;
          }








          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'J' )
          if ( cwd[strlen(cwd)-2] == 'P' )
          if ( cwd[strlen(cwd)-1] == 'G' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: JPG\n");
	     nrunwith( " feh -FZ  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }




	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-3] == '.' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 't' )
          {
	     /// "st" 
              if ( fexist( "/usr/local/bin/hatari" ) == 1 )
	       nrunwith(  "   /usr/local/bin/hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 

              else if ( fexist( "/usr/bin/hatari" ) == 1 )
	       nrunwith(  " /usr/bin/hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 

              else if ( fexist( "/usr/games/hatari" ) == 1 )
	       nrunwith(  " /usr/games/hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 

	     else
	       nrunwith(  " hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 

	      foundcmd = 1 ; 
   	      return 0; 
	  }







          //  "gb classic 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-3] == '.' )
          if ( cwd[strlen(cwd)-2] == 'g' )
          if ( cwd[strlen(cwd)-1] == 'b' )
          {

              if ( fexist( "/usr/bin/emulationstation" ) == 1 )
	         nrunwith(  "  nconfig --fb-gb " , fileselection ); 

              else if ( fexist( "/usr/local/bin/mednafen" ) == 1 )
	         nrunwith(  "  /usr/local/bin/mednafen    -vdriver sdl    " , fileselection ); 

              else if ( fexist( "/usr/games/mednafen" ) == 1 )
	         nrunwith(  "  /usr/games/mednafen   -vdriver sdl     " , fileselection ); 

              else if ( fexist( "/usr/bin/mednafen" ) == 1 )
	         nrunwith(  "  /usr/bin/mednafen    -vdriver sdl    " , fileselection ); 

              else 
	         nrunwith(  "  nconfig --gb " , fileselection ); 

	      foundcmd = 1 ; 
   	      return 0; 
	  }












          //  "gbc 
	  // mednafen    -vdriver sdl    -connect -netplay.host 10.0.0.4 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'g' )
          if ( cwd[strlen(cwd)-2] == 'b' )
          if ( cwd[strlen(cwd)-1] == 'c' )
          {
              if ( fexist( "/usr/local/bin/mednafen" ) == 1 )
	         nrunwith(  "  /usr/local/bin/mednafen    -vdriver sdl    " , fileselection ); 

              else if ( fexist( "/usr/games/mednafen" ) == 1 )
	         nrunwith(  "  /usr/games/mednafen   -vdriver sdl     " , fileselection ); 

              else if ( fexist( "/usr/bin/mednafen" ) == 1 )
	         nrunwith(  "  /usr/bin/mednafen    -vdriver sdl    " , fileselection ); 

              else 
	         nrunwith(  "  nconfig --gbc  " , fileselection ); 
	      foundcmd = 1 ; 
   	      return 0; 
	  }




	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'p' )
          {
	      nrunwith(  "  nconfig --fb-psp  " , fileselection ); 
	      foundcmd = 1 ; 
   	      return 0; 
	  }






          /// virtualbox "vbox extension  
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'v' )
          if ( cwd[strlen(cwd)-3] == 'b' )
          if ( cwd[strlen(cwd)-2] == 'o' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
	      nrunwith(  " virtualbox  " , fileselection ); 
	      foundcmd = 1 ; 
   	      return 0; 
	  }



	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == 'm' )
          {
	     /// "ppm"

             printf( "File: %s\n", cwd );
	     if ( fexist( "/usr/bin/gwenview" ) == 1 ) 
               nrunwith( " gwenview  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/xpaint" ) == 1 ) 
               nrunwith( " /usr/bin/xpaint  " , argv[ 1 ] );

	     else if ( fexist( "/usr/pkg/bin/xpaint" ) == 1 ) 
               nrunwith( " /usr/pkg/bin/xpaint  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mupdf" ) == 1 ) 
               nrunwith( " /usr/bin/mupdf  " , argv[ 1 ] );


	     else
               nrunwith( " xpaint  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }







	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 'b' )
          {
		  nrunwith(  " jmol " , fileselection ); 
		  foundcmd = 1 ; 
		  return 0; 
	  }



	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'j' )
          if ( cwd[strlen(cwd)-3] == 'x' )
          if ( cwd[strlen(cwd)-2] == 'y' )
          if ( cwd[strlen(cwd)-1] == 'z' )
          {
		  nrunwith(  " jmol " , fileselection ); 
		  foundcmd = 1 ; 
		  return 0; 
	  }











          //  gba  "gba  
	  // mednafen    -vdriver sdl    -connect -netplay.host 10.0.0.4 
	  // 
	  // on rpi4 gmba qt wont work maybe
          // pi retropie rpi4:  /usr/games/mgba -f -1 lego-Bionicle.gba
	  // 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'g' )
          if ( cwd[strlen(cwd)-2] == 'b' )
          if ( cwd[strlen(cwd)-1] == 'a' )
          {
              //if ( fexist( "/usr/games/mgba" ) == 1 )
	      //   nrunwith(  "  /usr/games/mgba -f -1  " , fileselection ); 

              if ( fexist( "/usr/games/mgba-qt" ) == 1 )
	         nrunwith(  "  /usr/games/mgba-qt   " , fileselection ); 

              else if ( fexist( "/usr/bin/mgba-qt" ) == 1 )
	         nrunwith(  "  /usr/bin/mgba-qt   " , fileselection ); 

              else if ( fexist( "/usr/local/bin/mednafen" ) == 1 )
	         nrunwith(  "  /usr/local/bin/mednafen    -vdriver sdl    " , fileselection ); 

              else if ( fexist( "/usr/games/mednafen" ) == 1 )
	         nrunwith(  "  /usr/games/mednafen   -vdriver sdl     " , fileselection ); 

              else if ( fexist( "/usr/bin/mednafen" ) == 1 )
	         nrunwith(  "  /usr/bin/mednafen    -vdriver sdl    " , fileselection ); 

              else 
	         nrunwith(  "  nconfig --gba  " , fileselection ); 

	      foundcmd = 1 ; 
   	      return 0; 
	  }










          //  redream  "gdi 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'g' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 'i' )
          {
	      // nrunwith(  " /usr/local/bin/redream " , fileselection ); 
              if ( fexist( "/usr/bin/emulationstation" ) == 1 )
	          nrunwith(  " nconfig --fb-dreamcast " , fileselection ); 
	      else
	          nrunwith(  "  nconfig --dreamcast  " , fileselection ); 

	      foundcmd = 1 ; 
   	      return 0; 
	  }






          //  PPSSPPSDL  "psp 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'p' )
          {
	      nrunwith(  " PPSSPPSDL " , fileselection ); 
	      foundcmd = 1 ; 
   	      return 0; 
	  }





	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
	      /// "psx 
              printf( " PS ONE for iso ...\n" ); 
	      nrunwith(  " nconfig --fb-psx " , fileselection ); 
	      foundcmd = 1 ; 
   	      return 0; 
	  }



	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'z' )
          if ( cwd[strlen(cwd)-2] == '6' )
          if ( cwd[strlen(cwd)-1] == '4' )
          {
	      /// "z64 
	      nrunwith(  " nconfig --fb-z64 " , fileselection ); 
	      foundcmd = 1 ; 
   	      return 0; 
	  }


	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'n' )
          if ( cwd[strlen(cwd)-2] == '6' )
          if ( cwd[strlen(cwd)-1] == '4' )
          {
	      /// "n64 
	      nrunwith(  " nconfig --fb-n64 " , fileselection ); 
	      foundcmd = 1 ; 
   	      return 0; 
	  }




	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'i' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'o' )
          {
	      /// "iso" 
              printf( " PS ONE for iso ...\n" ); 
              printf( " ...Dolphin EMU for Gamecube  ...\n" ); 
              printf( " ... DC  ...\n" ); 
	      printf( " ..... or PPSSPPSDL ... \n"  ); 

              if ( fexist( "/usr/games/PCSX2" ) == 1 ) 
	          nrunwith(  "   /usr/games/PCSX2  " , fileselection ); 


              else if ( ( fexist( "/usr/bin/emulationstation" ) == 1 )
              &&  ( strcmp( fbasename( currentpath ) , "psp" ) == 0 )  ) 
	      {
	          printf( " >>> Emu Retro PPSSPPSDL (psp directory) ... \n"  ); 
	          nrunwith(  " nconfig --fb-psp " , fileselection ); 
	      }

              else if ( fexist( "/usr/bin/emulationstation" ) == 1 )
	          nrunwith(  " nconfig --fb-psx " , fileselection ); 

              else if ( fexist( "/opt/retropie/libretrocores/lr-ppsspp/ppsspp_libretro.so" ) == 1 ) 
	          nrunwith(  " nconfig --fb-psp " , fileselection ); 

              else if ( fexist( "/usr/local/bin/PPSSPPSDL" ) == 1 )
	          nrunwith(  " PPSSPPSDL " , fileselection ); 

              else if ( fexist( "/usr/bin/dolphin-emu-nogui" ) == 1 )
	         nrunwith(  " /usr/bin/dolphin-emu-nogui  " , fileselection ); 

              else if ( fexist( "/usr/games/dolphin-emu" ) == 1 )
	         nrunwith(  " /usr/games/dolphin-emu -e " , fileselection ); 

	      else 
	         nrunwith(  " dolphin-emu  " , fileselection ); 

	      foundcmd = 1 ; 
   	      return 0; 
	  }








	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'd' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'k' )
	  // "dsk amstrad cpc  
          {
		strncpy( cmdi , "    /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-caprice32/cap32_libretro.so --config /opt/retropie/configs/amstradcpc/retroarch.cfg  " , PATH_MAX );
		strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , fileselection , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , "     --appendconfig /dev/shm/retroarch.cfg   " , PATH_MAX - strlen( cmdi ) -1 );
		nsystem( cmdi );
		foundcmd = 1 ; 
		return 0; 
	  }






          /*
	    strncpy( cmdi , "    /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-snes9x2010/snes9x2010_libretro.so --config /opt/retropie/configs/snes/retroarch.cfg  " , PATH_MAX );
	    strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
	    strncat( cmdi , argv[ 2 ] , PATH_MAX - strlen( cmdi ) -1 );
	    strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
	    strncat( cmdi , "    --appendconfig /dev/shm/retroarch.cfg  " , PATH_MAX - strlen( cmdi ) -1 );
	    nsystem( cmdi );
	  */
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 's' )
          if ( cwd[strlen(cwd)-2] == 'f' )
          if ( cwd[strlen(cwd)-1] == 'c' )
	  // "sfc snes 
          {
		strncpy( cmdi , "    /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-snes9x2010/snes9x2010_libretro.so --config /opt/retropie/configs/snes/retroarch.cfg  " , PATH_MAX );
		strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , fileselection , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , "     --appendconfig /dev/shm/retroarch.cfg   " , PATH_MAX - strlen( cmdi ) -1 );
		nsystem( cmdi );
		foundcmd = 1 ; 
		return 0; 
	  }


	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 's' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 'c' )
	  // "smc snes 
          {
		strncpy( cmdi , "    /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-snes9x2010/snes9x2010_libretro.so --config /opt/retropie/configs/snes/retroarch.cfg  " , PATH_MAX );
		strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , fileselection , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , "     --appendconfig /dev/shm/retroarch.cfg   " , PATH_MAX - strlen( cmdi ) -1 );
		nsystem( cmdi );
		foundcmd = 1 ; 
		return 0; 
	  }







	  // "a26"  atari 2600 
	  //  snprintf( charo , sizeof( charo ), "    /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-stella2014/stella2014_libretro.so --config /opt/retropie/configs/atari2600/retroarch.cfg   \"%s\" --appendconfig /dev/shm/retroarch.cfg  " , argv[ 2 ] ); 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'a' )
          if ( cwd[strlen(cwd)-2] == '2' )
          if ( cwd[strlen(cwd)-1] == '6' )
	  // "a26 atari 2600 
          {
	        printf( " ATARI 2600 emu ...\n" ); 
		strncpy( cmdi , "  /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-stella2014/stella2014_libretro.so --config /opt/retropie/configs/atari2600/retroarch.cfg  " , PATH_MAX );
		strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , fileselection , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , "  --appendconfig /dev/shm/retroarch.cfg   " , PATH_MAX - strlen( cmdi ) -1 );
		nsystem( cmdi );
		foundcmd = 1 ; 
		return 0; 
	  }





          /// 
          // i386 ~$ wine /opt/origin/origin-calc.exe  "z:\\$(pwd)/file.opj" 
	  // file /opt/origin-calc.tar.gz 
	  /// 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == 'j' )
          {
	      /// "opj" 
              //snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 

	      printf(  " > FILE OPJ (requires wine).\n" ); 
	      printf(  " > Current path: %s\n",    getcwd( cwd , PATH_MAX )     );
	      printf ( " > Needs for it wine and /opt/origin/origin-calc.exe \n" ); 

              if ( ( fexist( "/usr/bin/wine" ) == 1 ) && ( fexist( "/opt/origin/origin-calc.exe" ) == 1 ) )
	      {
                 snprintf( foostr, sizeof( foostr ) , " wine /opt/origin/origin-calc.exe  \"z:%s\\%s\"  ",  getcwd( cwd , PATH_MAX )     , fileselection ); 
		 nsystem( foostr ); 
	         //nrunwith(  " /opt/origin/origin-calc.exe  \"z:\\\\$(pwd)/\"" , fileselection ); 
	      }

	      else 
	      {
	         nrunwith(  "  scidavis  " , fileselection ); 
	      }
	      foundcmd = 1 ; 
   	      return 0; 
	  }


	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'O' )
          if ( cwd[strlen(cwd)-2] == 'P' )
          if ( cwd[strlen(cwd)-1] == 'J' )
          {
	      /// "opj" 
              //snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 

	      printf(  " > FILE OPJ (requires wine).\n" ); 
	      printf(  " > Current path: %s\n",    getcwd( cwd , PATH_MAX )     );
	      printf ( " > Needs for it wine and /opt/origin/origin-calc.exe \n" ); 

              if ( ( fexist( "/usr/bin/wine" ) == 1 ) && ( fexist( "/opt/origin/origin-calc.exe" ) == 1 ) )
	      {
                 snprintf( foostr, sizeof( foostr ) , " wine /opt/origin/origin-calc.exe  \"z:%s\\%s\"  ",  getcwd( cwd , PATH_MAX )     , fileselection ); 
		 nsystem( foostr ); 
	         //nrunwith(  " /opt/origin/origin-calc.exe  \"z:\\\\$(pwd)/\"" , fileselection ); 
	      }

	      else 
	      {
	         nrunwith(  "  scidavis  " , fileselection ); 
	      }
	      foundcmd = 1 ; 
   	      return 0; 
	  }











          // new 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 's' )
          if ( cwd[strlen(cwd)-2] == 't' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
	     /// "stx" 
              if ( fexist( "/usr/local/bin/hatari" ) == 1 )
	       nrunwith(  " /usr/local/bin/hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 

              else if ( fexist( "/usr/bin/hatari" ) == 1 )
	       nrunwith(  " /usr/bin/hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 

              else if ( fexist( "/usr/games/hatari" ) == 1 )
	       nrunwith(  " /usr/games/hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 

	     else
	       nrunwith(  " hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 
	       foundcmd = 1 ; 
		  return 0; 
	  }




          // new adf 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'a' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 'f' )
          {
	      /// "adf" 
              //if ( fexist( "/usr/games/fs-uae" ) == 0 )
              //else if ( fexist( "/usr/bin/emulationstation" ) == 1 )
	      //    nrunwith(  " /usr/local/bin/nconfig --amiga  " , fileselection ); 
              if ( fexist( "/usr/bin/fs-uae" ) == 1 )
	        nrunwith(  " /usr/bin/fs-uae  " , fileselection ); 

              else if ( fexist( "/usr/games/fs-uae" ) == 1 )
	        nrunwith(  " /usr/games/fs-uae  " , fileselection ); 

	      else
	        nrunwith(  "  nconfig --fb-amiga  " , fileselection ); 
	       ///nrunwith(  " fs-uae  " , fileselection ); 
              foundcmd = 1 ; 
	      return 0; 
           }



          // new md for megadrive
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-3] == '.' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 'd' )
          {
	      nrunwith(  "  nconfig --fb-megadrive " , fileselection ); 
              foundcmd = 1 ; 
	      return 0; 
          }






	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'a' )
          {
	     /// "msa" 
	  //	  nrunwith(  " /usr/local/bin/hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 
              if ( fexist( "/usr/local/bin/hatari" ) == 1 )
	       nrunwith(  " /usr/local/bin/hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 

              else if ( fexist( "/usr/bin/hatari" ) == 1 )
	       nrunwith(  " /usr/bin/hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 

              else if ( fexist( "/usr/games/hatari" ) == 1 )
	       nrunwith(  " /usr/games/hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 

	     else
	       nrunwith(  " hatari -t ~/tos102uk.img  --disk-a  " , fileselection ); 
	       foundcmd = 1 ; 
		  return 0; 
	  }








          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 't' )
          if ( cwd[strlen(cwd)-1] == 's' )
          {
	     //// "ots 
             printf( "File: %s\n", cwd );
             printf( "Ext: ots file\n");
	     nrunwith( " libreoffice --norestore " , argv[ 1 ] );
	     //nsystem(  foostr ); 
	     foundcmd = 1; 
	     return 0;
          }



          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'd' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'c' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
	     //// "docx 
             printf( "File: %s\n", cwd );
             printf( "Ext: docx file\n");
	     nrunwith( " libreoffice --norestore " , argv[ 1 ] );
	     nsystem(  foostr ); 
	     foundcmd = 1; 
	     return 0;
          }









          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'x' )
          if ( cwd[strlen(cwd)-2] == 'l' )
          if ( cwd[strlen(cwd)-1] == 's' )
          {
             printf( "File: %s\n", cwd );
	     //// "xls 
             //nrunwith( " libreoffice    --norestore   " , argv[ 1 ] );
             printf( "Ext: xls\n");
             printf( "Ext: -xls\n");

             // strncpy( libreoffice_parameter_start, "  --nodefault  --nolockcheck --nologo --norestore --nosplash  ", PATH_MAX ); 
	     if ( fexist( "/usr/bin/libreoffice" ) == 1 ) 
	     {
                //snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
                //snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		printf( " Libreoffice \n" ); 
		//nsystem(  foostr ); 
	        nrunwith( " libreoffice  --norestore  " , argv[ 1 ] );
	     }
	     else
	     {
		printf( " Gnumeric \n" ); 
	        nrunwith( " gnumeric  " , argv[ 1 ] );
	     }
	     foundcmd = 1; 
	     return 0;
          }





          ///
	  // "xlsx" 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'x' )
          if ( cwd[strlen(cwd)-3] == 'l' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
             printf( "File: %s\n", cwd );
	     nrunwith( " libreoffice  --norestore  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }




          ///
	  // "ppt" 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == 't' )
          {
             printf( "File: %s\n", cwd );
	     nrunwith( " libreoffice  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }



          ///
	  // "pptx" 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'p' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 't' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
             printf( "File: %s\n", cwd );
	     nrunwith( " libreoffice  --norestore  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }















          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 's' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: ods\n");
	     nrunwith( " libreoffice  --norestore  " , argv[ 1 ] );
              // --norestore 
	     //snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
	     //nsystem(  foostr ); 
	     /*
	     if ( fexist( "/usr/bin/libreoffice" ) == 1 ) 
	     {
	        //nrunwith( " libreoffice  " , argv[ 1 ] );
                snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		nsystem(  foostr ); 
	     }
	     else
	        nrunwith( " gnumeric  " , argv[ 1 ] );
		*/
	     foundcmd = 1; 
	     return 0;
          }


          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 'p' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: odp\n");
	     nrunwith( " libreoffice --norestore  " , argv[ 1 ] );
	     //nrunwith( " libreoffice  " , argv[ 1 ] );
	     /*
	     {
	        //nrunwith( " libreoffice  " , argv[ 1 ] );
                snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		nsystem(  foostr ); 
	     }
	     */
	     foundcmd = 1; 
	     return 0;
          }



          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == '5' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: md5\n");
	     nrunwith( " fledit  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 't' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 'b' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: tdb\n");
	     nrunwith( " fledit  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }







          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'd' )
          if ( cwd[strlen(cwd)-3] == 'v' )
          if ( cwd[strlen(cwd)-2] == 'j' )
          if ( cwd[strlen(cwd)-1] == 'u' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: dvju\n");
	     nrunwith( " mupdf  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }








          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 'p' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: odp\n");
	     //nrunwith( " libreoffice  " , argv[ 1 ] );
	     {
	        //nrunwith( " libreoffice  " , argv[ 1 ] );
                snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		nsystem(  foostr ); 
	     }
	     foundcmd = 1; 
	     return 0;
          }










          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'A' )
          if ( cwd[strlen(cwd)-2] == 'V' )
          if ( cwd[strlen(cwd)-1] == 'I' )
          { 
	     /// "AVI" large chars
             printf( "File: %s\n", cwd );
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }


          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'r' )
          if ( cwd[strlen(cwd)-2] == 'e' )
          if ( cwd[strlen(cwd)-1] == 'c' )
          { 
	     /// "rec" small chars
             printf( "File: %s\n", cwd );
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }















          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == 'g' )
          { 
	     /// "mpg" small chars
             printf( "File: %s\n", cwd );
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'm' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'e' )
          if ( cwd[strlen(cwd)-1] == 'g' )
          { 
	     /// "mpeg" small chars
             printf( "File: %s\n", cwd );
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }




	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'r' )
          if ( cwd[strlen(cwd)-2] == 'e' )
          if ( cwd[strlen(cwd)-1] == 'c' )
          { 
	     /// "rec" small chars
             printf( "File: %s\n", cwd );
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }
















	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'M' )
          if ( cwd[strlen(cwd)-2] == 'O' )
          if ( cwd[strlen(cwd)-1] == 'V' )
          { 
	     /// "MOV" small chars  // from phone older 
             printf( "File: %s\n", cwd );
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }




	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'w' )
          if ( cwd[strlen(cwd)-3] == 'e' )
          if ( cwd[strlen(cwd)-2] == 'b' )
          if ( cwd[strlen(cwd)-1] == 'p' )
          { 
	     /// "webp 
             printf( "WEBP File: %s\n", cwd );
	     if ( fexist( "/usr/bin/chromium" ) == 1 ) 
               nrunwith( " chromium " , argv[ 1 ] );
	     else if ( fexist( "/usr/bin/chromium-browser" ) == 1 ) 
               nrunwith( " chromium-browser " , argv[ 1 ] );
	     else 
               nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'w' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 'v' )
          { 
	     /// "wmv" small chars
             printf( "File: %s\n", cwd );
	     if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     else
	       movie_player( argv[ 1 ] );    
	     foundcmd = 1; 
	     return 0;
          }






          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'f' )
          if ( cwd[strlen(cwd)-2] == 'l' )
          if ( cwd[strlen(cwd)-1] == 'v' )
          { 
	     /// "flv" large chars
             printf( "File: %s\n", cwd );
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }


          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'g' )
          if ( cwd[strlen(cwd)-1] == 'v' )
          { 
	     /// ogv" large chars
             printf( "File: %s\n", cwd );
             nrunwith( " mplayer  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }



          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'g' )
          if ( cwd[strlen(cwd)-1] == 'g' )
          { 
	     /// ogg" large chars
             printf( "File: %s\n", cwd );
             nrunwith( " mplayer  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }






          /// "bmp 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'b' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 'p' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: bmp\n");
	     if ( fexist( "/usr/bin/feh" ) == 1 ) 
               nrunwith( " /usr/bin/feh " , argv[ 1 ] );
	     else 
	       nrunwith( " mupdf  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }












          if ( cwd[strlen(cwd)-4] == '.' )
          if ( ( cwd[strlen(cwd)-3] == 'w' ) || ( cwd[strlen(cwd)-3] == 'W' ) )
          if ( ( cwd[strlen(cwd)-2] == 'a' ) || ( cwd[strlen(cwd)-2] == 'A' ) )
          if ( ( cwd[strlen(cwd)-1] == 'd' ) || ( cwd[strlen(cwd)-1] == 'D' ) )
          {
	     /// wad  "WAD  "wad 
	     /// wad  "WAD  "wad"   
             printf( "File: %s\n", cwd );
             printf( "File argv,1: %s\n", argv[ 1 ] );

	     // NOTE: lzdoom on raspberry pi 4 shall be working, while on pi3, it is zdoom!

	     if ( fexist( "/opt/gzdoom/gzdoom" ) == 1 ) 
	     {
                 printf( " gzdoom " );
	         nrunwith( "  /opt/gzdoom/gzdoom    ", argv[ 1 ] );
	     }

	     else if ( fexist( "/opt/retropie/ports/lzdoom/lzdoom" ) == 1 ) 
	     {
                 printf( " LZDOOM " );
		 printf( " Raspberry PI 4.\n" ); 
	         nrunwith( "  /opt/retropie/ports/lzdoom/lzdoom +set fullscreen 1 -iwad  doom2.wad -file    ", argv[ 1 ] );
	     }

	     else if ( fexist( "/opt/retropie/ports/zdoom/zdoom" ) == 1 ) 
	     {
                 printf( " ZDOOM " );
		 printf( " Raspberry PI 3.\n" ); 
	         nrunwith( "  /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -iwad  doom2.wad -file    ", argv[ 1 ] );
	     }


             //////////
             //////////
	     else if ( fexist( "/opt-copy/retropie/ports/zdoom/zdoom" ) == 1 ) 
	     {
                 printf( " ZDOOM (copy)" );
	         nrunwith( "  /opt-copy/retropie/ports/zdoom/zdoom +set fullscreen 1 -iwad  doom2.wad -file    ", argv[ 1 ] );
	     }


	     else if ( fexist( "/usr/local/bin/cmzdoom" ) == 1 ) 
	     {
                 printf( " ZDOOM (copy, custom cmzdoom)" );
	         nrunwith( "  /usr/local/bin/cmzdoom  +set fullscreen 1 -iwad  doom2.wad -file    ", argv[ 1 ] );
	     }



	     else if ( fexist( "/usr/games/prboom-plus" ) == 1 ) 
	     {
                 printf( " PRBOOM " );
                 nrunwith( "    /usr/games/prboom-plus -iwad doom2.wad  -file " , argv[ 1 ] );
	     }
	     else 
	     {
                 printf( " PRBOOM " );
                 nrunwith( "    prboom-plus -iwad doom2.wad  -file " , argv[ 1 ] );
	     }
	     foundcmd = 1; 
	     return 0;
          }











          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'k' )
          if ( cwd[strlen(cwd)-1] == '3' )
          {
	     /// "pk3  
	     // 
	     /*
		     PK3 and for WADS:
		     pi       14154  0.0  0.0   1888   408 tty1     S+   16:19   0:00 sh -c    /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -iwad  doom2.wad -file     "PRCP.wad" 
		     pi       14155 88.4  4.8 108312 37396 tty1     Rl+  16:19   5:23 /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -iwad doom2.wad -file PRCP.wad
	     */
             printf( "File: %s\n", cwd );
             printf( "File argv,1: %s\n", argv[ 1 ] );

	     if ( fexist( "/opt/gzdoom/gzdoom" ) == 1 ) 
	     {
                 printf( " gzdoom " );
	         nrunwith( "  /opt/gzdoom/gzdoom    ", argv[ 1 ] );
	     }

	     else if ( fexist( "/opt/retropie/ports/zdoom/zdoom" ) == 1 ) 
	     {
                 printf( " ZDOOM " );
	         nrunwith( "  /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -iwad  doom2.wad -file    ", argv[ 1 ] );
	     }

	     else if ( fexist( "/opt/retropie/ports/lzdoom/lzdoom" ) == 1 ) 
	     {
                 printf( " LZDOOM " );
	         nrunwith( "  /opt/retropie/ports/lzdoom/lzdoom +set fullscreen 1 -iwad  doom2.wad -file    ", argv[ 1 ] );
	     }

	     else if ( fexist( "/usr/games/prboom-plus" ) == 1 ) 
	     {
                 printf( " PRBOOM " );
                 nrunwith( "    /usr/games/prboom-plus -iwad doom2.wad  -file " , argv[ 1 ] );
	     }
	     else 
	     {
                 printf( " PRBOOM " );
                 nrunwith( "    prboom-plus -iwad doom2.wad  -file " , argv[ 1 ] );
	     }
	     foundcmd = 1; 
	     return 0;
      }



	  /// //nsystem( "     /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -iwad  doom2.wad -file     minecraftdoom.pk3  " ); 
	  //nsystem( "     /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -iwad  doom2.wad -file     minecraftdoom.pk3  " );  
	  // ncmdwith( "   /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -iwad  doom2.wad -file    " ,  charo );
	  //nrunwith(  "  /usr/local/bin/mednafen    -vdriver sdl    " , fileselection ); 
          // void nrunwith( char *mycmd, char *myfile )
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'k' )
          if ( cwd[strlen(cwd)-1] == '3' )
          { 
	     /// pk3"  zdoom pk3 map 
             printf( "File: %s\n", cwd );
             //nrunwith( " ngame -zdoom " , argv[ 1 ] );
             printf( "> Attempts gzdoom or attempts zdoom on retropie ...\n" ); 

	     //if ( fexist( "/opt/gzdoom/gzdoom" ) == 1 ) 
             //   nrunwith( " /opt/gzdoom/gzdoom  " , argv[ 1 ] );

	     if ( fexist( "/opt/retropie/ports/zdoom/zdoom" ) == 1 )   // pi3
	         nrunwith( "   /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -iwad  doom2.wad -file    " ,  fileselection );

	     else if ( fexist( "/opt/retropie/ports/lzdoom/lzdoom" ) == 1 )  // pi4 
	         nrunwith( "   /opt/retropie/ports/lzdoom/lzdoom +set fullscreen 1 -iwad  doom2.wad -file    " ,  fileselection );

	     foundcmd = 1; 
	     return 0;
          }














    ////////////////////////////////////////////////////////
    //id-20200820-094228
    ////////////////////////////////////////////////////////
    if ( argc == 3 )
    if ( ( strcmp( argv[1] , "-gb" ) ==  0 ) 
    || ( strcmp( argv[1] , "--gngb" ) ==  0 ) 
    || ( strcmp( argv[1] , "--gb" ) ==  0 ) )
    {
             printf( "File: %s\n", cwd );
             printf( "Ext: gb\n");
             strncpy( cwd , "    gngb  -s  --color_filter    " , PATH_MAX );
             strncat( cwd , " " , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , " \"" , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd ,  argv[ 2 ] , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , "\"    -f -o     --res=1600x1000  " , PATH_MAX - strlen( cwd ) -1 );
	     printf( "CMD %s\n" , cwd );
             nsystem( cwd ); 
             return 0;
    }










    ////////////////////////////////////////////////////////
    if ( argc == 2)
    if ( strcmp( argv[1] , "" ) !=  0 ) 
    {
          strncpy( cwd, argv[1] ,  PATH_MAX );

          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'b' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 'r' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: bmr\n");
             nrunwith( " flnotepad  " , argv[ 1 ] );
	     return 0;
          }



          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'l' )
          if ( cwd[strlen(cwd)-2] == 'y' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
	     /// lyx
             printf( "File: %s\n", cwd );
             nrunwith( " lyx " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'g' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: msg\n");
             nrunwith( " flview " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0; 
          }









	  ////////////////////////////////////////////////////////
	  // Extension gb for gameboy using Mednafen, if exists
	  ////////////////////////////////////////////////////////
          if ( cwd[strlen(cwd)-3] == '.' )
          if ( cwd[strlen(cwd)-2] == 'g' )
          if ( cwd[strlen(cwd)-1] == 'b' )
	  if ( foundcmd == 0 ) 
	  {
	     driver_mednafen = 1; 
	  }
	  ///////////////////////////////
          if ( cwd[strlen(cwd)-3] == '.' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'g' )
	  if ( foundcmd == 0 ) 
	  {
	     driver_mednafen = 1; 
	  }
	  ///////////////////////////////








          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == '4' )
	  //// "mp4 
	  //// "mp4"  
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: mp4\n");

	     if ( fexist( "/usr/bin/mpv" ) == 1 ) 
	       movie_player( argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " /usr/bin/mplayer -fs -zoom " , argv[ 1 ] );

	     else if ( fexist( "/usr/local/bin/mplayer" ) == 1 ) 
               nrunwith( " /usr/local/bin/mplayer -fs -zoom " , argv[ 1 ] );

	     else if ( fexist( "/usr/pkg/bin/mplayer" ) == 1 ) 
               nrunwith(       " /usr/pkg/bin/mplayer -fs -zoom " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/vlc " ) == 1 ) 
               nrunwith( " /usr/bin/vlc " , argv[ 1 ] );


	     else if ( fexist( "/usr/bin/mpv " ) == 1 ) 
               nrunwith( " /usr/bin/mpv " , argv[ 1 ] );

             // nomadbsd eventually 
	     else if ( fexist( "/usr/bin/parole" ) == 1 ) 
               nrunwith( " /usr/bin/parole -i " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/ffplay" ) == 1 ) 
               nrunwith( " /usr/bin/ffplay " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/totem " ) == 1 ) 
               nrunwith( " /usr/bin/totem " , argv[ 1 ] );

	     else 
	       movie_player( argv[ 1 ] );
               //nrunwith( " vlc " , argv[ 1 ] );

	     foundcmd = 1; 
	     return 0;
          }











          if ( cwd[strlen(cwd)-3] == '.' )
          if ( cwd[strlen(cwd)-2] == 't' )
          if ( cwd[strlen(cwd)-1] == 's' )
	  //// "ts"  
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: ts\n");

	     if ( fexist( "/usr/bin/mpv" ) == 1 ) 
	       movie_player( argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " /usr/bin/mplayer -fs -zoom " , argv[ 1 ] );

	     else if ( fexist( "/usr/local/bin/mplayer" ) == 1 ) 
               nrunwith( " /usr/local/bin/mplayer -fs -zoom " , argv[ 1 ] );

	     else if ( fexist( "/usr/pkg/bin/mplayer" ) == 1 ) 
               nrunwith(       " /usr/pkg/bin/mplayer -fs -zoom " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/vlc " ) == 1 ) 
               nrunwith( " /usr/bin/vlc " , argv[ 1 ] );


	     else if ( fexist( "/usr/bin/mpv " ) == 1 ) 
               nrunwith( " /usr/bin/mpv " , argv[ 1 ] );

             // nomadbsd eventually 
	     else if ( fexist( "/usr/bin/parole" ) == 1 ) 
               nrunwith( " /usr/bin/parole -i " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/ffplay" ) == 1 ) 
               nrunwith( " /usr/bin/ffplay " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/totem " ) == 1 ) 
               nrunwith( " /usr/bin/totem " , argv[ 1 ] );

	     else 
	       movie_player( argv[ 1 ] );
               //nrunwith( " vlc " , argv[ 1 ] );

	     foundcmd = 1; 
	     return 0;
          }








          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'M' )
          if ( cwd[strlen(cwd)-2] == 'P' )
          if ( cwd[strlen(cwd)-1] == '4' )
	  //// "MP4 
	  //// "MP4"  
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: MP4\n");
	     if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " mplayer -ao null -fs -zoom " , argv[ 1 ] );
	       else 
               movie_player(  argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }



	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'a' )
          if ( cwd[strlen(cwd)-2] == 't' )
          if ( cwd[strlen(cwd)-1] == 'r' )
          {
	     // "atr for atari800 
	     // "atr"  
	     printf( " >== ATR ===>  ... \n" ); 
             printf( "File: %s\n", fileselection );

             if ( ( fexist( "/usr/local/bin/atari800pp" ) == 1 ) || ( fexist( "/usr/bin/atari800pp" ) == 1 ) ) 
	     {
		  printf( "   atari800pp -OsXlPath ~/atarixl.rom -Image.1  file.xex \n" ); 
		  printf( " atari800pp -OsXlPath ~/atarixl.rom -Image.1  file.xex \n" ); 
		  printf( " (sound fails): atari800 -xlxe_rom ~/BIOS/atarixl.rom -nosound -cart  file... \n" ); 
		  //  atari800pp -OsXlPath ~/atarixl.rom -Image.1  file.xex
		  printf( "File: %s\n", fileselection );
                  nrunwith(  "   atari800pp -OsXlPath  ~/atarixl.rom -Image.1    " , fileselection ); 
	      }
	      else 
	         nrunwith(  "  nconfig --fb-atari800  " , fileselection ); 
	         /*
		 strncpy( cmdi , "  /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-atari800/atari800_libretro.so --config /opt/retropie/configs/atari800/retroarch.cfg  " , PATH_MAX );
		 strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
		 strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
		 strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
		 strncat( cmdi , "    --appendconfig   /dev/shm/retroarch.cfg   " , PATH_MAX - strlen( cmdi ) -1 );
	         */
	     foundcmd = 1; 
	     return 0;
          }








	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'a' )
          if ( cwd[strlen(cwd)-2] == '2' )
          if ( cwd[strlen(cwd)-1] == '6' )
          {
	     // "a26 for atari2600  
	     printf( " >== A26 ===>  ... \n" ); 
             printf( "File: %s\n", fileselection );
	      nrunwith(  "  nconfig --fb-atari2600  " , fileselection ); 
	     foundcmd = 1; 
	     return 0;
          }




          if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'c' )
          if ( cwd[strlen(cwd)-2] == 'u' )
          if ( cwd[strlen(cwd)-1] == 'e' )
          {
	     // we assume we have a PS One ... 
	     printf( "=> cue CUE ... pcsxr \n" ); 
	     printf( " >>== NES ===>  ... \n" ); 
             printf( "File: %s\n", filestr );

	     if ( fexist( "/usr/bin/pcsxr" ) == 1 ) 
               nrunwith( " /usr/bin/pcsxr  -nogui -cdfile " , filestr );

	     else if ( fexist( "/usr/games/pcsxr" ) == 1 ) 
               nrunwith( " /usr/games/pcsxr  -nogui -cdfile " , filestr );

	     else if ( fexist( "/usr/local/bin/pcsxr" ) == 1 ) 
               nrunwith( " /usr/local/bin/pcsxr  -nogui -cdfile " , filestr );

             else
               nrunwith( "  /usr/local/bin/nconfig --fb-psx " , filestr );
	     foundcmd = 1; 
	     return 0;
          }







          //mednafen -video.driver sdl
	  //if ( driver_mednafen == 1 ) 
          // fceu -opengl 0  file.nes 
	  // "nes nes ""nes "nes"
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'n' )
          if ( cwd[strlen(cwd)-2] == 'e' )
          if ( cwd[strlen(cwd)-1] == 's' )
          {
	     /// nes"
	     //printf( "> Info: ...alt + shift + 1  (top left 1). to reset keys\n" );
	     printf( " >>== NES ===>  ... \n" ); 
             printf( "File: %s\n", cwd );

             if ( fexist(    "/usr/games/mednafen" ) == 1 ) 
                 nrunwith(   " /usr/games/mednafen  -video.driver sdl    " , argv[ 1 ] );

             else if ( fexist(    "/usr/games/fceu" ) == 1 ) 
                 nrunwith(   " /usr/games/fceu  -opengl 0  " , argv[ 1 ] );

             else if ( fexist(    "/usr/games/fceux-sdl" ) == 1 )               // special custom 
                 nrunwith(   " /usr/games/fceux-sdl -opengl 0 " , argv[ 1 ] );  // special custom 

             else if ( fexist(    "/usr/games/fceux" ) == 1 ) 
                 nrunwith(   " /usr/games/fceux  " , argv[ 1 ] );

             else if ( fexist(    "/usr/local/bin/fceux" ) == 1 )  // freebsd
                 nrunwith(   " /usr/local/bin/fceux  " , argv[ 1 ] );

             else if ( fexist(    "/usr/games/fceu" ) == 1 ) 
                 nrunwith(   " /usr/games/fceu  " , argv[ 1 ] );

             else if ( fexist(    "/usr/pkg/bin/mednafen" ) == 1 ) 
                 nrunwith(   "/usr/pkg/bin/mednafen  -video.driver sdl  " , argv[ 1 ] );

             else if ( fexist( "/usr/bin/mednafen" ) == 1 ) 
                 nrunwith( " /usr/bin/mednafen  -video.driver sdl  " , argv[ 1 ] );

             else if ( fexist( "/usr/games/mednafen" ) == 1 ) 
                 nrunwith(     "/usr/games/mednafen  -video.driver sdl  " , argv[ 1 ] );

	     else
                 nrunwith( "  nconfig --fb-nes  " , argv[ 1 ] );
                 // nrunwith( " mednafen -video.driver sdl  " , argv[ 1 ] );

	     foundcmd = 1; 
	     return 0;
          }



	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'w' )
          if ( cwd[strlen(cwd)-2] == 'a' )
          if ( cwd[strlen(cwd)-1] == 'v' )
          {
	     // "wav" 
             printf( "File: %s\n", cwd );
             printf( "Ext: wav \n");

	     if ( fexist( "/etc/wscons.conf" ) == 1 )     // netbsd 
              nrunwith( " audioplay  " , argv[ 1 ] );

	     else
              music_player( argv[ 1 ] );

             /*
	     else if ( fexist( "/usr/bin/cvlc " ) == 1 )     
              nrunwith( "  /usr/bin/cvlc   " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/audacious " ) == 1 )   
              nrunwith( "  /usr/bin/audacious   " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mplayer " ) == 1 )   
              nrunwith( "  /usr/bin/mplayer   " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/parole " ) == 1 )     
              nrunwith( "  /usr/bin/parole   " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/vlc " ) == 1 )     
              nrunwith( "  /usr/bin/vlc   " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mpv " ) == 1 )     
              nrunwith( "  /usr/bin/mpv   " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/aplay " ) == 1 )   
              nrunwith( "  /usr/bin/aplay   " , argv[ 1 ] );

	     else
              nrunwith( " mplayer  " , argv[ 1 ] );
	      */
	     foundcmd = 1; 
	     return 0;
          }








	  // "zip 
	  // "zip 
	  // "zip 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'z' )
          if ( cwd[strlen(cwd)-2] == 'i' )
          if ( cwd[strlen(cwd)-1] == 'p' )
          {
	     /// zip"
	     //printf( "> Info: ...alt + shift + 1  (top left 1). to reset keys\n" );
	     printf( " >== Zip for ARK, maybe NES ===>  ... \n" ); 
             printf( "File: %s\n", cwd );

             printf( " > ZIP, CURRENT PATH: %s\n" , fbasename( currentpath ) ); 
             ////Jprintf( " > Search ZIP or MAME : /usr/bin/mame %d\n" , fexist( "/usr/bin/mame" ) ); 
 	     //printf( "%s\n", fbasename( getcwd( cmdi , PATH_MAX ) ) );

             if ( fexist( "usr/bin/file-roller" ) == 1 ) 
                 nrunwith(   "   file-roller   " , argv[ 1 ] );

             else if ( strcmp( fbasename( currentpath ) , "mame-libretro" ) == 0 )  
                 nrunwith(   "   nconfig --fb-mame  " , argv[ 1 ] );

             else if ( strcmp( fbasename( currentpath ) , "nes" ) == 0 )  
                 nrunwith(   "   nconfig --fb-nes " , argv[ 1 ] );
	     
             else if ( fexist(    "/usr/bin/ark" ) == 1 ) 
                 nrunwith(   " /usr/bin/ark  " , argv[ 1 ] );
		 // running on opensuse

             else if ( fexist(    "/usr/games/mednafen" ) == 1 ) 
                 nrunwith(   " /usr/games/mednafen  -video.driver sdl    " , argv[ 1 ] );

             else if ( fexist(    "/usr/games/fceu" ) == 1 ) 
                 nrunwith(   " /usr/games/fceu  -opengl 0  " , argv[ 1 ] );

             else if ( fexist(    "/usr/games/fceux-sdl" ) == 1 )               // special custom 
                 nrunwith(   " /usr/games/fceux-sdl -opengl 0 " , argv[ 1 ] );  // special custom 

             else if ( fexist(    "/usr/games/fceux" ) == 1 ) 
                 nrunwith(   " /usr/games/fceux  " , argv[ 1 ] );


             else if ( fexist(    "/usr/local/bin/fceux" ) == 1 )  // freebsd
                 nrunwith(   " /usr/local/bin/fceux  " , argv[ 1 ] );

             else if ( fexist(    "/usr/games/fceu" ) == 1 ) 
                 nrunwith(   " /usr/games/fceu  " , argv[ 1 ] );

             else if ( fexist(    "/usr/pkg/bin/mednafen" ) == 1 ) 
                 nrunwith(   "/usr/pkg/bin/mednafen  -video.driver sdl  " , argv[ 1 ] );

             else if ( fexist( "/usr/bin/mednafen" ) == 1 ) 
                 nrunwith( " /usr/bin/mednafen  -video.driver sdl  " , argv[ 1 ] );

             else if ( fexist( "/usr/games/mednafen" ) == 1 ) 
                 nrunwith(     "/usr/games/mednafen  -video.driver sdl  " , argv[ 1 ] );

	     else
               nrunwith( " mednafen -video.driver sdl  " , argv[ 1 ] );

	     foundcmd = 1; 
	     return 0;
          }



	  // "rar" 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'r' )
          if ( cwd[strlen(cwd)-2] == 'a' )
          if ( cwd[strlen(cwd)-1] == 'r' )
          {
	     /// "rar"
             printf( "File: %s\n", cwd );

             if ( fexist(    "/usr/bin/ark" ) == 1 ) 
                 nrunwith(   " /usr/bin/ark  " , argv[ 1 ] );
		 // running on opensuse

	     foundcmd = 1; 
	     return 0;
          }



















          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'r' )
          if ( cwd[strlen(cwd)-2] == 'o' )
          if ( cwd[strlen(cwd)-1] == 'm' )
          if ( foundcmd == 0 ) 
          {

	     // we assume we have a PS One ... 
	     printf( "=> rom ROM ... with tilem2 for maths  \n" ); 
             nrunwith( "  tilem2 -r " , filestr );
	     foundcmd = 1; 
	     return 0;
          }







          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'i' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'o' )
          if ( foundcmd == 0 ) 
          {
	     /////////////////////////////////
	     /// iso file asking for dolphin  
	     /*
		nnsystem( "   export DISPLAY=:0 ; cd ; PCSX2  " ); 
                //nrunwith( " xterm -e dolphin-emu -e " , argv[ 1 ] );
		//// --nogui --fullscreen
	     */
	     /////////////////////////////////
             printf( "File (iso, ISO): %s\n", cwd );
	     if  ( strstr( filestr , "(PSN)" ) != 0 )   
	     {
               nrunwith( " PPSSPPSDL  --escape-exit --fullscreen --load " , filestr );
	     }
	     else if  ( strstr( filestr , "(PSX)" ) != 0 )   
	     {
               nrunwith( "  pcsxr  " , filestr );
	     }
	     else if  ( strstr( filestr , "(NGC)" ) != 0 )   
	     {
               nrunwith( "  dolphin-emu -e  " , filestr );
	     }
	     else
	     {
               nrunwith( " PCSX2  --nogui --fullscreen  " , argv[ 1 ] );
	     }
	     foundcmd = 1; 
	     return 0;
          }













	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'J' )
          if ( cwd[strlen(cwd)-2] == 'P' )
          if ( cwd[strlen(cwd)-1] == 'G' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: JPG\n");
             nrunwith( " mupdf " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }











          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'r' )
          if ( cwd[strlen(cwd)-2] == 'i' )
          if ( cwd[strlen(cwd)-1] == 's' )
          {
	     /// ris 
             printf( "File: %s\n", cwd );
             nrunwith( " flnotepad " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'a' )
          if ( cwd[strlen(cwd)-2] == 't' )
          if ( cwd[strlen(cwd)-1] == 'r' )
          {
	     /// atr for atari800   
	     // -xlxe_rom  ~/BIOS/atarixl.rom -fullscreen  -car   
             printf( "File: %s\n", cwd );
             nrunwith( " atari800     -xlxe_rom  ~/BIOS/atarixl.rom -fullscreen  -car   " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }




          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 's' )
          if ( cwd[strlen(cwd)-2] == 'c' )
          if ( cwd[strlen(cwd)-1] == 'p' )
          {
	     /// scp for sc  
             printf( "File: %s\n", cwd );
             nrunwith( " sc  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'c' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'v' )
          {
	     /// csv 
             printf( "File: %s\n", cwd );
	     if ( fexist( "/usr/bin/libreoffice" ) == 1 ) 
                 nrunwith( " libreoffice    --norestore   " , argv[ 1 ] );
	     else
                nrunwith( " flnotepad " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'h' )
          if ( cwd[strlen(cwd)-3] == 't' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 'l' )
          {
	     // "html"
             printf( "File: %s\n", cwd );
             printf( "Ext: html\n");
             nrunwith( "  dillo " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }


          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'd' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'c' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
	     // "docx"
             printf( "File: %s\n", cwd );
             printf( "Ext: docx\n");
	     if ( fexist( "/usr/bin/libreoffice" ) == 1 ) 
                nrunwith( " libreoffice    --norestore   " , argv[ 1 ] );
	     else if ( fexist( "/usr/bin/lowriter" ) == 1 ) 
                nrunwith( " lowriter  --norestore   " , argv[ 1 ] );
	     else
                nrunwith( " libreoffice --norestore   " , argv[ 1 ] );
	     //nrunwith( " libreoffice  " , argv[ 1 ] );
             ///snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
	     ///nsystem(  foostr ); 
	     foundcmd = 1; 
	     return 0;
          }









          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'i' )
          if ( cwd[strlen(cwd)-2] == 'n' )
          if ( cwd[strlen(cwd)-1] == 'i' )
          {
	     /// "ini"
             printf( "File: %s\n", cwd );
             nrunwith( " flnotepad " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }


          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'd' )
          if ( cwd[strlen(cwd)-2] == 'a' )
          if ( cwd[strlen(cwd)-1] == 't' )
          {
	     /// "dat"
             printf( "File: %s\n", cwd );
             nrunwith( " flnotepad " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }



          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 'r' )
          if ( cwd[strlen(cwd)-1] == 'k' )
          {
	     /// "mrk"
             printf( "File: %s\n", cwd );
             nrunwith( " flnotepad " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'r' )
          if ( cwd[strlen(cwd)-2] == 'e' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
	     /// "rex"
             printf( "File: %s\n", cwd );
             nrunwith( " flnotepad " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }


          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'f' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 'v' )
          {
	     /// fdv"
             printf( "File: %s\n", cwd );
             nrunwith( " flview  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }

          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'x' )
          if ( cwd[strlen(cwd)-2] == 'y' )
          if ( cwd[strlen(cwd)-1] == 'z' )
          {
	     /// xyz" for jmol
             printf( "File: %s\n", cwd );
             nrunwith( " jmol " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }






          // new 32x 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == '3' )
          if ( cwd[strlen(cwd)-2] == '2' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
	      nrunwith(  "  nconfig --fb-sega32x  " , fileselection ); 
              foundcmd = 1; 
	      return 0; 
           }



          // new sms 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 's' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 's' )
          {
	      /// "sms" 
	      // mastersystem, mednafen 
              //   -vdriver sdl     <-- not working on pi 
	      //  -video.driver sdl    
              if ( fexist( "/usr/games/mednafen" ) == 1 )
	        nrunwith(  " /usr/games/mednafen -video.driver sdl " , fileselection ); 

              else if ( fexist( "/usr/bin/mednafen" ) == 1 )
	        nrunwith(  " /usr/bin/mednafen -video.driver sdl " , fileselection ); 

	      else
	        nrunwith(  "  nconfig --fb-sms  " , fileselection ); 
              foundcmd = 1; 
	      return 0; 
           }




	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'l' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 't' )
          {
	     /// "lst"  list like listdir  
             printf( "File: %s\n", cwd );
	     if ( fexist( "/usr/local/bin/flnotepad" ) == 1 ) 
                nrunwith( " flnotepad " , argv[ 1 ] );
	     else if ( fexist( "/usr/bin/flnotepad" ) == 1 ) 
                nrunwith( " flnotepad " , argv[ 1 ] );
	     else if ( fexist( "/usr/bin/mousepad" ) == 1 ) 
                nrunwith( " mousepad " , argv[ 1 ] );
	     else
                nrunwith( " notepad " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }




	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 't' )
          if ( cwd[strlen(cwd)-2] == 'x' )
          if ( cwd[strlen(cwd)-1] == 't' )
          {
	     /// "txt"

             printf( "File: %s\n", cwd );
	     if ( fexist( "/usr/local/bin/flnotepad" ) == 1 ) 
                nrunwith( " flnotepad " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/flnotepad" ) == 1 ) 
                nrunwith( " flnotepad " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mousepad" ) == 1 ) 
                nrunwith( " mousepad " , argv[ 1 ] );
	     else
                nrunwith( " notepad " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }




          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'c' )
          if ( cwd[strlen(cwd)-2] == 'f' )
          if ( cwd[strlen(cwd)-1] == 'g' )
          {
	     /// "cfg"  
             printf( "File: %s\n", cwd );
	     if ( fexist( "/usr/local/bin/flnotepad" ) == 1 ) 
                nrunwith( " flnotepad " , argv[ 1 ] );
	     else if ( fexist( "/usr/bin/flnotepad" ) == 1 ) 
                nrunwith( " flnotepad " , argv[ 1 ] );
	     else if ( fexist( "/usr/bin/mousepad" ) == 1 ) 
                nrunwith( " mousepad " , argv[ 1 ] );
	     else
                nrunwith( " notepad " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }











          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'T' )
          if ( cwd[strlen(cwd)-2] == 'X' )
          if ( cwd[strlen(cwd)-1] == 'T' )
          {
	     /// TXT"
             printf( "File: %s\n", cwd );
             nrunwith( " flnotepad " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }











	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == '3' )
          if ( cwd[strlen(cwd)-2] == 'G' )
          if ( cwd[strlen(cwd)-1] == 'P' )
          { 
	     /// "3GP" upper chars
             printf( "File: %s\n", cwd );

	     if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/cvlc " ) == 1 ) 
               nrunwith( " cvlc " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mpv " ) == 1 ) 
               nrunwith( " mpv " , argv[ 1 ] );

	     else
               nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }




	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 'o' )
          if ( cwd[strlen(cwd)-1] == 'v' )
          { 
	     /// "mov" small chars
             printf( "== Movie Player == File: %s\n", cwd );
	     movie_player( argv[ 1 ] );    
	     foundcmd = 1; 
	     return 0;
          }




          // "avi 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'a' )
          if ( cwd[strlen(cwd)-2] == 'v' )
          if ( cwd[strlen(cwd)-1] == 'i' )
          { 
             printf( "== Movie Player AVI == File: %s\n", cwd );

	     if ( fexist( "/usr/bin/mpv" ) == 1 ) 
	       movie_player( argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " mplayer " , argv[ 1 ] );

	     else if ( fexist( "/etc/flmpv.fs" ) == 1 ) 
               nrunwith( " /usr/bin/mpv --fs " , argv[ 1 ] );

	     else
	       movie_player( argv[ 1 ] );    

	     foundcmd = 1; 
	     return 0;
          }










	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == '3' )
          if ( cwd[strlen(cwd)-2] == 'g' )
          if ( cwd[strlen(cwd)-1] == 'p' )
          { 
             printf( "== Movie 3gp Player == File: %s\n", cwd );

	     /// "3gp" small chars
	     // vlc does not maybe work on pi retro

	     if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " mplayer " , argv[ 1 ] );
               //nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     else if ( fexist( "/etc/flmpv.fs" ) == 1 ) 
               nrunwith( " /usr/bin/mpv --fs " , argv[ 1 ] );
	       //movie_player( argv[ 1 ] );    
	     else
	       movie_player( argv[ 1 ] );    

	     /* 
	     if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/cvlc " ) == 1 ) 
               nrunwith( " cvlc " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mpv " ) == 1 ) 
               nrunwith( " mpv " , argv[ 1 ] );

	     else
               nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
	     */
	     foundcmd = 1; 
	     return 0;
          }






          // "jpg" 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'j' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == 'g' )
          {
	     /// "jpg"
             printf( "File: %s\n", cwd );
             printf( "File: %s\n", cwd );
             printf( "Main Ext: jpg\n");

	     /// 
             printf( "File  /usr/bin/feh       : %d\n", fexist( "/usr/bin/feh"       ) );
             printf( "File  /usr/bin/gpicview  : %d\n", fexist( "/usr/bin/gpicview"  ) );
             printf( "File  /usr/bin/mupdf     : %d\n", fexist( "/usr/bin/mupdf"     ) );

	     if ( fexist( "/usr/bin/gpicview" ) == 1 ) 
               nrunwith( " /usr/bin/gpicview " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/feh" ) == 1 ) 
               nrunwith( " /usr/bin/feh -FZ  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/gpicview" ) == 1 ) 
               nrunwith( " /usr/bin/gpicview " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/eog" ) == 1 )  /// ubuntu ryzen 
               nrunwith( " /usr/bin/eog  " , argv[ 1 ] ); 

             else 
               //nrunwith( " mupdf " , argv[ 1 ] );
               procedure_pictureviewer( argv[ 1 ] ); 

	     foundcmd = 1; 
	     return 0;
          }




          // "JPG" 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'J' )
          if ( cwd[strlen(cwd)-2] == 'P' )
          if ( cwd[strlen(cwd)-1] == 'G' )
          {
	     /// "JPG"
             printf( "File: %s\n", cwd );
             printf( "File: %s\n", cwd );
             printf( "Main Ext: JPG\n");

	     /// 
             printf( "File  /usr/bin/feh       : %d\n", fexist( "/usr/bin/feh"       ) );
             printf( "File  /usr/bin/gpicview  : %d\n", fexist( "/usr/bin/gpicview"  ) );
             printf( "File  /usr/bin/mupdf     : %d\n", fexist( "/usr/bin/mupdf"     ) );

	     if ( fexist( "/usr/bin/gpicview" ) == 1 ) 
               nrunwith( " /usr/bin/gpicview " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/feh" ) == 1 ) 
               nrunwith( " /usr/bin/feh -FZ  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/gpicview" ) == 1 ) 
               nrunwith( " /usr/bin/gpicview " , argv[ 1 ] );
             else 
               nrunwith( " mupdf " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          // "vob"  
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'v' )
          if ( cwd[strlen(cwd)-2] == 'o' )
          if ( cwd[strlen(cwd)-1] == 'b' )
          {
               nrunwith( " mplayer -fs -zoom  " , argv[ 1 ] );
	       foundcmd = 1; 
	       return 0;
          }




          // setfont psf.gz
          // "psf.gz
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-7] == '.' )
          if ( cwd[strlen(cwd)-6] == 'p' )
          if ( cwd[strlen(cwd)-5] == 's' )
          if ( cwd[strlen(cwd)-4] == 'f' )
          if ( cwd[strlen(cwd)-3] == '.' )
          if ( cwd[strlen(cwd)-2] == 'g' )
          if ( cwd[strlen(cwd)-1] == 'z' )
          {
		  printf(   "Open PSF Console Font File: %s\n", cwd );
                  nrunwith( " setfont " , argv[ 1 ] );
		  foundcmd = 1; 
		  return 0;
          }




          // "png"  "png"  
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'n' )
          if ( cwd[strlen(cwd)-1] == 'g' )
          {
	     /// png"
             printf( "> Open PNG File (ncrun): %s\n", cwd );
             // /usr/bin/ristretto  on opensuse xfce4

	     if ( fexist( "/usr/bin/feh" ) == 1 )  
                nrunwith( " feh -FZ  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/gpicview" ) == 1 ) 
               nrunwith( " /usr/bin/gpicview " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mupdf" ) == 1 ) 
               nrunwith( " /usr/bin/mupdf " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/eog" ) == 1 ) 
               nrunwith( " /usr/bin/eog " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/feh" ) == 1 )  
                nrunwith( " feh -FZ  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/feh" ) == 1 ) 
               nrunwith( " /usr/bin/feh -FZ  " , argv[ 1 ] );

	     else if ( ( MYOS == 1 ) && ( fexist( "/usr/bin/zypper" ) == 1 )  && ( fexist( "/usr/bin/ristretto" ) == 1 ) )
	     {
                 nrunwith( " /usr/bin/ristretto " , argv[ 1 ] );
	     }
	     else if ( fexist( "/usr/bin/mupdf" ) == 1 )  
                nrunwith( " mupdf " , argv[ 1 ] );

	     else if ( fexist( "/usr/pkg/bin/mupdf" ) == 1 )  
                nrunwith( " mupdf " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/gwenview" ) == 1 )  
                nrunwith( " gwenview " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/feh" ) == 1 )  
                nrunwith( " feh " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mupdf" ) == 1 )  
                nrunwith( " /usr/bin/mupdf " , argv[ 1 ] );

	     else if ( fexist( "/usr/local/bin/mupdf" ) == 1 )  
                nrunwith( " /usr/local/bin/mupdf " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/eog" ) == 1 )  /// ubuntu ryzen 
               nrunwith( " /usr/bin/eog  " , argv[ 1 ] ); 

	     else if ( fexist( "/usr/bin/firefox" ) == 1 )  
                nrunwith( " /usr/bin/firefox " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/firefox-esr" ) == 1 )  
                nrunwith( " /usr/bin/firefox-esr " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/chromium" ) == 1 )  
                nrunwith( " /usr/bin/chromium " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/chromium-browser" ) == 1 )  
                nrunwith( " /usr/bin/chromium-browser " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/dillo" ) == 1 )  
                nrunwith( " /usr/bin/dillo " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/links2" ) == 1 )  
                nrunwith( " /usr/bin/links2 -g " , argv[ 1 ] );

    	     else
                nrunwith( " flimgview " , argv[ 1 ] );

	     foundcmd = 1; 
	     return 0;
          }









          // "gif"  
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'g' )
          if ( cwd[strlen(cwd)-2] == 'i' )
          if ( cwd[strlen(cwd)-1] == 'f' )
          {
             printf( "Open GIF File: %s\n", cwd );
             // /usr/bin/ristretto  on opensuse xfce4

	     if ( fexist( "/usr/bin/feh" ) == 1 )  
                nrunwith( " feh -FZ  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/gpicview" ) == 1 ) 
               nrunwith( " /usr/bin/gpicview " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/feh" ) == 1 )  
                nrunwith( " feh -FZ  " , argv[ 1 ] );

	     else if ( ( MYOS == 1 ) && ( fexist( "/usr/bin/zypper" ) == 1 )  
	          && ( fexist( "/usr/bin/ristretto" ) == 1 ) )
	     {
                 nrunwith( " /usr/bin/ristretto " , argv[ 1 ] );
	     }
	     else if ( fexist( "/usr/bin/mupdf" ) == 1 )  
                nrunwith( " mupdf " , argv[ 1 ] );

	     else if ( fexist( "/usr/pkg/bin/mupdf" ) == 1 )  
                nrunwith( " mupdf " , argv[ 1 ] );


	     else if ( fexist( "/usr/bin/feh" ) == 1 )  
                nrunwith( " feh " , argv[ 1 ] );

	     else if ( fexist( "/usr/local/bin/xweb" ) == 1 )   // work around on bsd 
                nrunwith( " /usr/local/bin/xweb -g " , argv[ 1 ] );


	     else if ( fexist( "/usr/bin/mupdf" ) == 1 )  
                nrunwith( " /usr/bin/mupdf " , argv[ 1 ] );

	     else if ( fexist( "/usr/local/bin/mupdf" ) == 1 )  
                nrunwith( " /usr/local/bin/mupdf " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/firefox" ) == 1 )  
                nrunwith( " /usr/bin/firefox " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/firefox-esr" ) == 1 )  
                nrunwith( " /usr/bin/firefox-esr " , argv[ 1 ] );

    	     else
                nrunwith( " flimgview " , argv[ 1 ] );

	     foundcmd = 1; 
	     return 0;
          }









	  // "mp3"
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == '3' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: mp3 \n");

	     if ( ( fexist( "/usr/bin/xterm" ) == 1 ) && ( fexist( "/usr/bin/mplayer" ) == 1 ) )  
               nrunwith( " mplayer " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mplayer" ) == 1 )   
               nrunwith( " mplayer " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mpg123" ) == 1 ) 
               nrunwith( " mpg123 " , argv[ 1 ] );

	     else if ( ( fexist( "/usr/bin/xterm" ) == 1 ) && ( fexist( "/usr/bin/mpg123" ) == 1 ) )  
               nrunwith( " xterm -e mpg123 " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mplayer" ) == 1 )   
               nrunwith( " mplayer " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mpg123" ) == 1 ) 
               nrunwith( " mpg123 " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/vlc" ) == 1 ) 
               nrunwith( " vlc  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/parole" ) == 1 ) 
               nrunwith( " parole -i " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/totem" ) == 1 ) 
               nrunwith( " totem " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mpv" ) == 1 ) 
               nrunwith( " mpv " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " mplayer " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/vlc" ) == 1 ) 
               nrunwith( " vlc " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
               nrunwith( " cvlc " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/parole" ) == 1 ) 
               nrunwith( " /usr/bin/parole -i " , argv[ 1 ] );

             else 
               //nrunwith( " mplayer " , argv[ 1 ] );
	       music_player(  argv[ 1 ] ); 

	     foundcmd = 1; 
	     return 0;
          }





	  // "MP3 
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'M' )
          if ( cwd[strlen(cwd)-2] == 'P' )
          if ( cwd[strlen(cwd)-1] == '3' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: mp3 \n");

	     if ( ( fexist( "/usr/bin/xterm" ) == 1 ) && ( fexist( "/usr/bin/mplayer" ) == 1 ) )  
               nrunwith( " xterm -e mplayer " , argv[ 1 ] );

	     else if ( ( fexist( "/usr/bin/xterm" ) == 1 ) && ( fexist( "/usr/bin/mpg123" ) == 1 ) )  
               nrunwith( " xterm -e mpg123 " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mplayer" ) == 1 )   
               nrunwith( " mplayer " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mpg123" ) == 1 ) 
               nrunwith( " mpg123 " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/vlc" ) == 1 ) 
               nrunwith( " vlc  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/parole" ) == 1 ) 
               nrunwith( " parole -i " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mpv" ) == 1 ) 
               nrunwith( " mpv " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " mplayer " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/vlc" ) == 1 ) 
               nrunwith( " vlc " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
               nrunwith( " cvlc " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/parole" ) == 1 ) 
               nrunwith( " /usr/bin/parole -i " , argv[ 1 ] );

             else 
               nrunwith( " mplayer " , argv[ 1 ] );

	     foundcmd = 1; 
	     return 0;
          }





	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == '4' )
          if ( cwd[strlen(cwd)-1] == 'a' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: m4a \n");
             nrunwith( " mplayer " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }



	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == '3' )
          if ( cwd[strlen(cwd)-1] == 'u' )
          {
             printf( "M3U File: %s\n", cwd );
             printf( "Ext: m3u \n");

	     if ( fexist( "/usr/bin/vlc" ) == 1 ) 
               nrunwith( " vlc  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/parole" ) == 1 ) 
               nrunwith( " parole  " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mpv" ) == 1 ) 
               nrunwith( " mpv " , argv[ 1 ] );

	     else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
               nrunwith( " mplayer " , argv[ 1 ] );

             else 
               nrunwith( " mplayer " , argv[ 1 ] );

	     foundcmd = 1; 
	     return 0;
          }








          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'P' )
          if ( cwd[strlen(cwd)-2] == 'N' )
          if ( cwd[strlen(cwd)-1] == 'G' )
          {
	     /// "PNG"
             printf( "File: %s\n", cwd );
             nrunwith( " mupdf " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          if ( cwd[strlen(cwd)-9] == '.' )
          if ( cwd[strlen(cwd)-8] == 'g' )
          if ( cwd[strlen(cwd)-7] == 'n' )
          if ( cwd[strlen(cwd)-6] == 'u' )
          if ( cwd[strlen(cwd)-5] == 'm' )
          if ( cwd[strlen(cwd)-4] == 'e' )
          if ( cwd[strlen(cwd)-3] == 'r' )
          if ( cwd[strlen(cwd)-2] == 'i' )
          if ( cwd[strlen(cwd)-1] == 'c' )
          {
	     ///  gnumeric   
             printf( "File: %s\n", cwd );
             nrunwith( " gnumeric " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }


          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'T' )
          if ( cwd[strlen(cwd)-2] == 'D' )
          if ( cwd[strlen(cwd)-1] == 'B' )
          {
	     /// TDB
             printf( "File: %s\n", cwd );
             nrunwith( " flnotepad  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }

          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 't' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 'b' )
          {
	     /// tdb
             printf( "File: %s\n", cwd );
             nrunwith( " flnotepad  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }

          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'O' )
          if ( cwd[strlen(cwd)-2] == 'C' )
          if ( cwd[strlen(cwd)-1] == 'M' )
          {
	     /// ocm 
             printf( "File: %s\n", cwd );
             nrunwith( " flnotepad  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }

          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'c' )
          if ( cwd[strlen(cwd)-1] == 'm' )
          {
	     /// ocm 
             printf( "File: %s\n", cwd );
             nrunwith( " flnotepad  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }


          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'k' )
          if ( cwd[strlen(cwd)-1] == 'z' )
          {
	     /// pkz"
             printf( "File: %s\n", cwd );
             nrunwith( " ark  " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }










          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'x' )
          if ( cwd[strlen(cwd)-3] == 'l' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: xlsx\n");
	     nrunwith( " libreoffice --norestore " , argv[ 1 ] );
	     nsystem(  foostr ); 
	     foundcmd = 1; 
	     return 0;
          }
	  





















          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'd' )
          if ( cwd[strlen(cwd)-2] == 'o' )
          if ( cwd[strlen(cwd)-1] == 'c' )
          {
	     // "doc"
             printf( "File: %s\n", cwd );
             printf( "Ext: doc\n");
	     /*
	     if ( fexist( "/usr/bin/calligrawords" ) == 1 ) 
                nrunwith( " calligrawords  " , argv[ 1 ] );
	     else 
	     {
                //nrunwith( " libreoffice  " , argv[ 1 ] );
                     snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		     nsystem(  foostr ); 
	     }
	     */
             nrunwith( " libreoffice    --norestore   " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }

          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'D' )
          if ( cwd[strlen(cwd)-2] == 'O' )
          if ( cwd[strlen(cwd)-1] == 'C' )
          {
	     // "DOC"
             printf( "File: %s\n", cwd );
             printf( "Ext: DOC\n");
	     if ( fexist( "/usr/bin/calligrawords" ) == 1 ) 
                nrunwith( " calligrawords  " , argv[ 1 ] );
	     else 
                //nrunwith( " libreoffice  " , argv[ 1 ] );
		{
                     snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		     nsystem(  foostr ); 
		     }
	     foundcmd = 1; 
	     return 0;
          }





          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'b' )
          if ( cwd[strlen(cwd)-2] == 'a' )
          if ( cwd[strlen(cwd)-1] == 't' )
          {
	     // "bat"
             printf( "File: %s\n", cwd );
             printf( "Ext: bat\n");
             nrunwith( " fledit " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }






          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'p' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 't' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: pptx\n");
             if ( strstr( currentpath , "nfs" ) != 0 ) 
             {
		   if ( fexist( "/usr/bin/calligrastage" ) == 1 ) 
		     nrunwith( " calligrastage  " , argv[ 1 ] );
		   else 
		   {
		     //nrunwith( " libreoffice  " , argv[ 1 ] );
                     snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		     nsystem(  foostr ); 
		   }
             }
	     else 
 	     {  //  nrunwith( " libreoffice  " , argv[ 1 ] );
                     snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		     nsystem(  foostr ); 
	     }
	     foundcmd = 1; 
	     return 0;
          }








          ///
	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == 't' )
          {
	     // "ppt"
             printf( "File: %s\n", cwd );
             printf( "Ext: ppt\n");
	     if ( fexist( "/usr/bin/calligrastage" ) == 1 ) 
                nrunwith( " calligrastage  " , argv[ 1 ] );
	     else 
	     {
                //nrunwith( " libreoffice  " , argv[ 1 ] );
                     snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		     nsystem(  foostr ); 
		     }
	     foundcmd = 1; 
	     return 0;
          }











          ///////////////////////////////////////////
          ///////////////////////////////////////////
          ///////////////////////////////////////////
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == 'm' )
          {
		  /// ppm
		  printf( "File: %s\n", cwd );
		  strncpy( cmdi , " " , PATH_MAX );
		  strncat( cmdi , " ppmtojpeg " , PATH_MAX - strlen( cmdi ) -1 );
		  strncat( cmdi , "  " , PATH_MAX - strlen( cmdi ) -1 );
		  strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
		  strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
		  strncat( cmdi ,  argv[ 1 ]  , PATH_MAX - strlen( cmdi ) -1 );
		  strncat( cmdi , "\"   > /tmp/image.jpg  " , PATH_MAX - strlen( cmdi ) -1 );
		  strncat( cmdi , " ;  du -hs /tmp/image.jpg ;  mupdf    /tmp/image.jpg  " , PATH_MAX - strlen( cmdi ) -1 );
		  printf( "CMD %s\n" , cmdi );
		  nsystem( cmdi ); 
		  foundcmd = 1; 
		  return 0;
          }










          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'J' )
          if ( cwd[strlen(cwd)-2] == 'P' )
          if ( cwd[strlen(cwd)-1] == 'G' )
          {
	     /// jpg"
             printf( "File: %s\n", cwd );
             nrunwith( " mupdf " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }



          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'l' )
          if ( cwd[strlen(cwd)-2] == 'y' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
	     /// lyx
             printf( "File: %s\n", cwd );
             nrunwith( " lyx " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }



          /////////////////////////////////
          if ( cwd[strlen(cwd)-8] == '.' )
          if ( cwd[strlen(cwd)-7] == 'p' )
          if ( cwd[strlen(cwd)-6] == 'l' )
          if ( cwd[strlen(cwd)-5] == 'a' )
          if ( cwd[strlen(cwd)-4] == 'n' )
          if ( cwd[strlen(cwd)-3] == 'n' )
          if ( cwd[strlen(cwd)-2] == 'e' )
          if ( cwd[strlen(cwd)-1] == 'r' )
	  if ( foundcmd == 0 ) 
          {
             printf( "File: %s  \n", cwd );
             printf( "Ext: planner \n");
             nrunwith( " planner   " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }





          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'd' )
          if ( cwd[strlen(cwd)-3] == 'j' )
          if ( cwd[strlen(cwd)-2] == 'v' )
          if ( cwd[strlen(cwd)-1] == 'u' )
	  if ( foundcmd == 0 ) 
          {
             printf( "File: %s  \n", cwd );
             printf( "Ext: djvu \n");
             nrunwith( " mupdf   " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }




          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 't' )
          if ( cwd[strlen(cwd)-2] == 'e' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: tex\n");
             nrunwith( " flnotepad  " , argv[ 1 ] );
	     return 0;
          }




          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'B' )
          if ( cwd[strlen(cwd)-2] == 'M' )
          if ( cwd[strlen(cwd)-1] == 'P' )
	  if ( foundcmd == 0 ) 
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: BMP\n");
             nrunwith( " mupdf   " , argv[ 1 ] );
	     foundcmd = 1; 
	     return 0;
          }







          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'v' )
          if ( cwd[strlen(cwd)-2] == 'i' )
          if ( cwd[strlen(cwd)-1] == 'd' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: vid\n");
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
          }





          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 'o' )
          if ( cwd[strlen(cwd)-1] == 'v' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: mov\n");
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
          }











          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'W' )
          if ( cwd[strlen(cwd)-2] == 'A' )
          if ( cwd[strlen(cwd)-1] == 'V' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: WAV \n");
	     if ( fexist( "/etc/wscons.conf" ) == 1 )     // netbsd 
              nrunwith( " audioplay  " , argv[ 1 ] );
	       else
              nrunwith( " mplayer  " , argv[ 1 ] );
          }







          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'l' )
          if ( cwd[strlen(cwd)-1] == 't' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: plt gnuplot \n");
             nrunwith( " gnuplot  -p   " , argv[ 1 ] );
          }






          // epub
          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'e' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'u' )
          if ( cwd[strlen(cwd)-1] == 'b' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: epub \n");
             nrunwith( " mupdf " , argv[ 1 ] );
          }

          /// BMP 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'b' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 'p' )
          {
             printf( "File: %s\n", cwd );
             nrunwith( " xpaint  " , argv[ 1 ] );
          }


          /// TSV 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'T' )
          if ( cwd[strlen(cwd)-2] == 'S' )
          if ( cwd[strlen(cwd)-1] == 'V' )
          {
             printf( "File: %s\n", cwd );
             nrunwith( " mplayer -fs -zoom  " , argv[ 1 ] );
          }


          /// XWD 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'x' )
          if ( cwd[strlen(cwd)-2] == 'w' )
          if ( cwd[strlen(cwd)-1] == 'd' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: xwd\n");
	     printf( "xwd: xwdview \n" );
             nrunwith( " xwdview -in " , argv[ 1 ] );
	     /// this was feh, but we have now mupdf 
          }


          // BMP
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'B' )
          if ( cwd[strlen(cwd)-2] == 'M' )
          if ( cwd[strlen(cwd)-1] == 'P' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: BMP \n");
             nrunwith( " mupdf " , argv[ 1 ] );
          }











          // doc
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'd' )
          if ( cwd[strlen(cwd)-2] == 'o' )
          if ( cwd[strlen(cwd)-1] == 'c' )
          {
                 //nrunwith( " libreoffice  ", argv[1] );
                     snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		     nsystem(  foostr ); 
          }







          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'w' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 'v' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: wmv\n");
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
          }

          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'g' )
          if ( cwd[strlen(cwd)-1] == 'g' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: ogg\n");
             nrunwith( " mplayer -zoom " , argv[ 1 ] );
          }


          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'a' )
          if ( cwd[strlen(cwd)-2] == 'v' )
          if ( cwd[strlen(cwd)-1] == 'i' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: avi\n");
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
          }


          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'V' )
          if ( cwd[strlen(cwd)-2] == 'O' )
          if ( cwd[strlen(cwd)-1] == 'B' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: vob\n");
             nrunwith( " mplayer -fs -zoom " , argv[ 1 ] );
          }


	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-3] == '.' )
          if ( cwd[strlen(cwd)-2] == 'm' )
          if ( cwd[strlen(cwd)-1] == 'd' )
          {
	     // "md"
             printf( "File: %s\n", cwd );
             printf( "Ext: md\n");
             nrunwith( "  fledit  " , argv[ 1 ] );
	     foundcmd = 1; 
          }


	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'i' )
          if ( cwd[strlen(cwd)-2] == 'e' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: iex\n");
             nrunwith( "  fledit  " , argv[ 1 ] );
	     foundcmd = 1; 
          }



          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'c' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'c' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: csc\n");
             printf( "Classical Spreadsheet for ncurses\n");
             nrunwith( "  fledit  " , argv[ 1 ] );
          }



          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'x' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == 'm' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: xpm\n");
             nrunwith( " mupdf   " , argv[ 1 ] );
          }

          if ( cwd[strlen(cwd)-3] == '.' )
          if ( cwd[strlen(cwd)-2] == 'x' )
          if ( cwd[strlen(cwd)-1] == 'm' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: xm\n");
             nrunwith( " milkytracker  " , argv[ 1 ] );
          }


          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'l' )
          if ( cwd[strlen(cwd)-2] == 'k' )
          if ( cwd[strlen(cwd)-1] == 'c' )
          {  // lkc 
             printf( "File: %s\n", cwd );
             printf( "Ext: lkc \n");
             nrunwith( " lkcalc  " , argv[ 1 ] );
          }





          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'n' )
          if ( cwd[strlen(cwd)-1] == 'g' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: png\n");
             nrunwith( " mupdf  " , argv[ 1 ] );
          }



          //// .dat dat data dat file, ex. ascii from a testing device
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'd' )
          if ( cwd[strlen(cwd)-2] == 'a' )
          if ( cwd[strlen(cwd)-1] == 't' )
	  if ( foundcmd == 0 ) 
          {  
             printf( "File: %s\n", cwd );
             printf( "Ext: dat\n");
             printf( "Format: Data dat file format \n" );
             nrunwith( " fledit  " , argv[ 1 ] );
	     foundcmd = 1; 
          }

          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'B' )
          if ( cwd[strlen(cwd)-2] == 'M' )
          if ( cwd[strlen(cwd)-1] == 'P' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: BMP\n");  
             strncpy( cwd , "    xpaint    " , PATH_MAX );
             strncat( cwd , " \"" , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd ,  argv[ 1 ] , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , "\"  " , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , "  " , PATH_MAX - strlen( cwd ) -1 );
	     printf( "CMD %s\n" , cwd );
             nsystem( cwd ); 
	     foundcmd = 1; 
	  }



          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'h' )
          if ( cwd[strlen(cwd)-2] == 't' )
          if ( cwd[strlen(cwd)-1] == 'm' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: htm\n");  

             if ( fexist( "/usr/bin/dillo" ) == 1 ) 
                strncpy( cwd , "    dillo    " , PATH_MAX );
	     else if ( fexist( "/usr/bin/chromium" ) == 1 ) 
                strncpy( cwd , "    chromium    " , PATH_MAX );
	     else if ( fexist( "/usr/bin/chromium-browser" ) == 1 ) 
                strncpy( cwd , "    chromium-browser    " , PATH_MAX );
             else if ( fexist( "/usr/pkg/bin/dillo" ) == 1 ) 
                strncpy( cwd , "    dillo    " , PATH_MAX );
	     else
                strncpy( cwd , "   links     " , PATH_MAX );

             strncat( cwd , " \"" , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd ,  argv[ 1 ] , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , "\"  " , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , "  " , PATH_MAX - strlen( cwd ) -1 );
	     printf( "CMD %s\n" , cwd );
             nsystem( cwd ); 
	     foundcmd = 1; 
	  }






          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'o' )
          if ( cwd[strlen(cwd)-2] == 'd' )
          if ( cwd[strlen(cwd)-1] == 't' )
	  if ( foundcmd == 0 ) 
          {
	     /// "odt"
             printf( "File: %s\n", cwd );
             printf( "Ext: odt\n");
             //nrunwith( " libreoffice " , argv[ 1 ] );
                 //   snprintf( foostr, PATH_MAX , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		 //    nsystem(  foostr ); 
             nrunwith( " libreoffice " , argv[ 1 ] );
	     foundcmd = 1; 
          }




          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 's' )
          if ( cwd[strlen(cwd)-2] == 'v' )
          if ( cwd[strlen(cwd)-1] == 'g' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: svg\n");  
             if ( fexist( "/usr/bin/inkscape" ) == 1 ) 
                strncpy( cwd , "    inkscape    " , PATH_MAX );
	     else
                strncpy( cwd , "    inkscape    " , PATH_MAX );
             strncat( cwd , " \"" , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd ,  argv[ 1 ] , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , "\"  " , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , "  " , PATH_MAX - strlen( cwd ) -1 );
	     printf( "CMD %s\n" , cwd );
             nsystem( cwd ); 
	     foundcmd = 1; 
	  }






          ///////////////////////
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 's' )
          if ( cwd[strlen(cwd)-1] == 'd' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: psd\n");  // PSD
             strncpy( cwd , "    convert   " , PATH_MAX );
             strncat( cwd , " \"" , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd ,  argv[ 1 ] , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , "\"  /tmp/1.jpg   " , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , " ; mupdf /tmp/1.jpg " , PATH_MAX - strlen( cwd ) -1 );
	     printf( "CMD %s\n" , cwd );
             nsystem( cwd ); 
	     foundcmd = 1; 
	  }







          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'e' )
          if ( cwd[strlen(cwd)-2] == 'p' )
          if ( cwd[strlen(cwd)-1] == 's' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: eps\n");  // EPS eps 
             strncpy( cwd , "    cat   " , PATH_MAX );
             strncat( cwd , " \"" , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd ,  argv[ 1 ] , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , "\"  | epstopdf --filter > /tmp/test.pdf  " , PATH_MAX - strlen( cwd ) -1 );
             strncat( cwd , " ; mupdf /tmp/test.pdf " , PATH_MAX - strlen( cwd ) -1 );
	     printf( "CMD %s\n" , cwd );
             nsystem( cwd ); 
	     foundcmd = 1; 
	  }

          


          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'c' )
          if ( cwd[strlen(cwd)-2] == 'x' )
          if ( cwd[strlen(cwd)-1] == 'x' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: cxx\n");
             nrunwith( " fledit  " , argv[ 1 ] );
          }





	  if ( foundcmd == 0 ) 
          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'x' )
          if ( cwd[strlen(cwd)-2] == 'l' )
          if ( cwd[strlen(cwd)-1] == 's' )
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: xls\n");
	     if ( fexist( "/usr/bin/libreoffice" ) == 1 )  
               //nrunwith( " libreoffice  " , argv[ 1 ] );
	       {
	             // added sizeof 
                     snprintf( foostr, sizeof( foostr ) , "  libreoffice %s \"%s\"  ", libreoffice_parameter_start , argv[ 1 ] ); 
		     nsystem(  foostr ); 
	     }
	     else
               nrunwith( " gnumeric  " , argv[ 1 ] );
	     foundcmd = 1; 
          }





          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'f' )
          if ( cwd[strlen(cwd)-2] == 'l' )
          if ( cwd[strlen(cwd)-1] == 'd' )
	  if ( foundcmd == 0 ) 
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: fld\n");
             nrunwith( " fledit  " , argv[ 1 ] );
	     foundcmd = 1; 
          }

          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 'i' )
          if ( cwd[strlen(cwd)-1] == 'd' )
	  if ( foundcmd == 0 ) 
          {
             printf( "File: %s\n", cwd );
             printf( "Ext: mid\n");
             printf( "Player: timidity\n");
             nrunwith( "   timidity   " , argv[ 1 ] );
	     foundcmd = 1;
	  }

          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'c' )
          if ( cwd[strlen(cwd)-2] == 'a' )
          if ( cwd[strlen(cwd)-1] == 'r' )
	  if ( foundcmd == 0 ) 
          {
	     /// car"
             printf( "File: %s\n", cwd );
             printf( "Ext: car\n");
             nrunwith( " gdis   " , argv[ 1 ] );
	     foundcmd = 1; 
          }



          if ( cwd[strlen(cwd)-4] == '.' )
          if ( cwd[strlen(cwd)-3] == 'm' )
          if ( cwd[strlen(cwd)-2] == 'k' )
          if ( cwd[strlen(cwd)-1] == 'v' )
	  if ( foundcmd == 0 ) 
          {
	     /// mkv"
             printf( "File: %s\n", cwd );
             printf( "Ext: mkv\n");
             nrunwith( " mplayer -loop  0 -fs -zoom  " , argv[ 1 ] );
	     foundcmd = 1; 
          }






          if ( cwd[strlen(cwd)-5] == '.' )
          if ( cwd[strlen(cwd)-4] == 'j' )
          if ( cwd[strlen(cwd)-3] == 'p' )
          if ( cwd[strlen(cwd)-2] == 'e' )
          if ( cwd[strlen(cwd)-1] == 'g' )
	  if ( foundcmd == 0 ) 
          {
	     /// jpeg"
             printf( "File: %s\n", cwd );
             nrunwith( " mupdf " , argv[ 1 ] );
	     foundcmd = 1; 
          }







    }

    return 0; 
}  


